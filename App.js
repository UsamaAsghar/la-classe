import React, { Component } from 'react';
import { SafeAreaView, StatusBar } from 'react-native'
import AppContainer from './src/startUp';
// import { Provider } from 'react-redux';
import { Provider as StoreProvider } from 'react-redux';
import { STORE, PERSISTOR } from './src/store/storeConfig';
import { DefaultTheme, Provider as PaperProvider } from 'react-native-paper';
import { PersistGate } from 'redux-persist/integration/react';
import * as Util from './src/services';
import SplashScreen from 'react-native-splash-screen';

// const { store, persistor } = configureStore()
// passwordforkeystore = 'instantsolutions'

const theme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    primary: '#000',
    accent: '#000',
  },
};

export default class App extends Component {
  componentDidMount() {
    SplashScreen.hide()
  }

  render() {
    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: 'rgba(0,0,0,0.8)' }}>
        <StatusBar backgroundColor="rgba(0,0,0,0.8)" barStyle='dark-content' />
        <StoreProvider store={STORE}>
          <PersistGate persistor={PERSISTOR}>
            <PaperProvider theme={theme}>
              <AppContainer
                ref={navigatorRef => {
                  Util.setTopLevelNavigator(navigatorRef);
                }}
              />
            </PaperProvider>
          </PersistGate>
        </StoreProvider >
      </SafeAreaView>
    );
  }
}
