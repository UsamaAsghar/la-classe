export const loginStrings = {
    signIn: "SIGN IN",
    signUp: "SIGN UP",
    subTitle: "Sign into your account",
    emailPlaceHolder: 'Enter email address',
    passwordPlaceHolder: 'Enter password',
    forgetPassword: 'Forget your password?',
    noAccount: 'YOU DO NOT HAVE AN ACCOUNT?',
}
export const registorStrings = {
    headerText: "Registration",
    subTitle: "Creation of your user account",
    studentWord: "Student",
    teacherWord: "Teaher",
    namePlaceHolder: "Full Name",
    phoneNoPlaceHolder: "Phone number",
    establishmentPlaceHolder: "Establishment",
    locationPlaceHolder: "Choose a city",
    emailPlaceHolder: 'Enter email address',
    passwordPlaceHolder: 'Enter password',
    confirmPasswordPlaceHolder: 'Confirm password',
    generalDetail: 'By clicking on the "Creat my account" button, you indicate that you have read and accepted the ',
    generalCondition: 'General Conditions of Use.',
    creatAccount: 'CREATE MY ACCOUNT',
    alreadyHaveAccount: 'Already have an account?',
}
export const forgetPasswordStrings = {
    forgetPassword: "FORGET YOUR PASSWORD",
    subTitleText: "Please enter the email address associated with your Laclasse account to receive the password reset link.",
    Confirm: "CONFIRM"
}
export const newPasswordStrings = {
    newPassword: "ENTER YOUR PASSWORD",
    placeHolder:"********",
    Confirm: "CONFIRM"
}
export const classCreationStrings = {
    headerText: "Jhon Doe",
    title: "Class creation",
    classNameString: "Class Name",
    establishmentString: "Establishment",
    cityString: "City",
    confirmString: "CONFIRM",
    uploadString:"UPLOAD",
    classCreation:"Class creation",
    addingStudent:"Adding Students"
}
export const profileStrings = {
    header: "MY PROFILE",
    name: "Jhon Doe",
    email: "hello.jhondoe@gmail.com",
    address: "cell number",
    state: "city name",
    changePassword: "Change Password",
    deleteAccount:"Are You Sure You Want To Delete Your Account",
    Save: "SAVE",
    Yes:"Yes",
    No:"NO"
}
export const courseCreationStrings = {
    title: "Course Creation",
    courseName: "Course Name",
    description: "Description",
    classes: "Choose from your classes",
    startCourse: "Start the course now",
    timeStartText: "Start date and time *",
    timeEndText: "End date and time *",
    timeAndDate: "April 19,2020 23:13"
}
export const myClassesStrings = {
    title: "MY CLASSES",
    classesDetail: "0  Classes",
    noClasses: "No Classes",
    creatClass: "CREATE CLASS"

}
export const myClassesCardStrings = {
    class: "Class One",
    establishment: "Establishment",
    establishmentName: "SS",
    city: "City",
    cityName: "Mediouna",
    participants: "Participants",
    totalParticipants: "08",
    toAccess: "TO ACCEESS"
}
export const myCoursesStrings = {
    title: "MY COURSES",
    coursesDetail: "0  Courses",
    noCourses: "No Courses",
    creatCourse: "CREATE COURSE",
    toAccess: "TO ACCEESS",
    archivedText:"Archived Course"
}
export const myCoursesCardStrings = {
    course: "Course One",
    createdby: "Created by:",
    creatorsname: "  Francisco",
    startCourse: "Start the course now",
    startCourseLater:'Start the course latter',
    documentDetail: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore",
    onlineAddress: "http:// commpte.laclasse.ma/live/qk4df",
    copy: "COPY",
    testclass: "Test Class",
    start:"START",
    download:"DOWNLOAD",
}
export const HomeScreenStrings = {
    title:"Welcome , John Doe",
    subTitle:"Get Started",
    creatClass:"CREATE A CLASS",
    creatLesson:"CREATE A LESSON",
    statistics:"Statistics",
    totalNoOfStudents:"Total number of courses",
    noOfCourses:"22",
    totalNoOfClasses:"Total number of classes",
    noOfClasses:"10",
    totalNoOfMyStudents:"Total number of my students",
    noOfMyStudents:"75",
    nextCourse:"The next courses",
    noOfnextCourses:"05",
    upcommingEvent:"Upcomming Events",
    seemore:"See more",
    eventName:"Event name",
    month:"JULY",
    date:"17",
    day:"Saturday",
    time:"TIME",
    hour:"10:00",
    timeDimention:"Am",
    tutorials:"tutorials",
    usage:"How to use the platform?",
    startCourse:"How to start a course?"
}
export const MyArchivesStrings ={
    title:"MY ARCHIVES",
    subtitle:"03 Archived Lessons",
    courseOne:"Course One",
    createdby: "Created by:",
    creatorsname: "  Francisco",
    startCourse: "Start the course now",
    documentDetail: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore",
    archived:"ARCHIVED",
    back:"MyCourses"

}
export const AddStudentsStrings ={
    title:"Adding students",
    subtitle:"* By default, the class takes the city and establishes your profile. ",
     namePlaceHolder:"Full Name",
     emailPlaceHolder:"Email Address",
     add:"ADD",
     studentEmailAddress:"harrymlasher@teleworm.us",
     confirm:"CONFIRM",
     detailText:"These students will receive the invitation in their email address."
}
export const ListOfStudentsStrings ={
    listOfStudents:"List of Students",
    classes:"Classes",
    archivedCourses:"Archived Courses",
    title:"LIST OF STUDENTS",
    add:"ADD",
    name:"David J. Abbey",
    email:"hellodavid@gmail.com",
    ////modal Strings
    deleteStudent:"Delete student",
    detailText:"Are you sure you want to remove the 'Mathew' student from this Class?",
    confirm:"Confirm",
    export:"EXPORT"

}
export const YourProgramStrings ={
    title:"Your program",
    creatLesson:"CREAT A LESSON"
}
