import * as TYPES from '../../actions/types';

const initialState = {
    loading: false,
    startCourse: null,
    error: null,
    isSuccess: false,
    isFailure: false
}

const startcoursesReducer = (state = initialState, actions) => {
    switch (actions.type) {
        case TYPES.START_COURSES_REQUEST:
            return {
                ...state,
                loading: true,
                startCourse: null,
                isSuccess: false,
                isFailure: false
            }
        case TYPES.START_COURSES_SUCCESS:
            // console.log('startCourses Reducer===:',actions.user);
            return {
                ...state,
                loading: false,
                startCourse: actions.user,
                isSuccess: true,
                isFailure: false
            }
        case TYPES.START_COURSES_FAILURE:
            return {
                ...state,
                loading: false,
                startCourse: null,
                error: actions.error,
                isSuccess: false,
                isFailure: true
            }
        default:
            return state
    }
}
export default startcoursesReducer;
