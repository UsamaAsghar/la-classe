import * as TYPES from '../../actions/types';

const initialState = {
    loading: false,
    coursesData: null,
    error: null,
    isSuccess: false,
    isFailure: false
}
const getcoursesReducer = (state = initialState, actions) => {
    switch (actions.type) {
        case TYPES.GET_COURSES_REQUEST:
            return {
                ...state,
                loading: true,
                coursesData: null,
                isSuccess: false,
                isFailure: false
            }
        case TYPES.GET_COURSES_SUCCESS:
            console.log('getCourses Reducer===:',actions.user);
            return {
                ...state,
                loading: false,
                coursesData: actions.user,
                isSuccess: true,
                isFailure: false
            }
        case TYPES.GET_COURSES_FAILURE:
            return {
                ...state,
                loading: false,
                coursesData: null,
                error: actions.error,
                isSuccess: false,
                isFailure: true
            }
        default:
            return state
    }
}
export default getcoursesReducer;
