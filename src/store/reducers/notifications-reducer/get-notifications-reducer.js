import * as TYPES from '../../actions/types';

const initialState = {
    loading: false,
    notificationsData: null,
    error: null,
    isSuccess: false,
    isFailure: false
}
const getnotificationsReducer = (state = initialState, actions) => {
    switch (actions.type) {
        case TYPES.GET_NOTIFICATIONS_REQUEST:
            return {
                ...state,
                loading: true,
                notificationsData: null,
                isSuccess: false,
                isFailure: false
            }
        case TYPES.GET_NOTIFICATIONS_SUCCESS:
            console.log('getNotifications Reducer===:',actions.user);
            return {
                ...state,
                loading: false,
                notificationsData: actions.user,
                isSuccess: true,
                isFailure: false
            }
        case TYPES.GET_NOTIFICATIONS_FAILURE:
            return {
                ...state,
                loading: false,
                notificationsData: null,
                error: actions.error,
                isSuccess: false,
                isFailure: true
            }
        default:
            return state
    }
}
export default getnotificationsReducer;
