import * as TYPES from '../../actions/types';

const initialState = {
    loading: false,
    participantslistData: null,
    error: null,
    isSuccess: false,
    isFailure: false
}
const getParticipantslistReducer = (state = initialState, actions) => {
    switch (actions.type) {
        case TYPES.GET_PARTICIPANTSLIST_REQUEST:
            return {
                ...state,
                loading: true,
                participantslistData: null,
                isSuccess: false,
                isFailure: false
            }
        case TYPES.GET_PARTICIPANTSLIST_SUCCESS:
            console.log('getParticipantsList Reducer===:',actions.user);
            return {
                ...state,
                loading: false,
                participantslistData: actions.user,
                isSuccess: true,
                isFailure: false
            }
        case TYPES.GET_PARTICIPANTSLIST_FAILURE:
            return {
                ...state,
                loading: false,
                participantslistData: null,
                error: actions.error,
                isSuccess: false,
                isFailure: true
            }
        default:
            return state
    }
}
export default getParticipantslistReducer;
