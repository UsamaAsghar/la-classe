import * as TYPES from '../../actions/types';

const initialState = {
    loading: false,
    classesData: null,
    error: null,
    isSuccess: false,
    isFailure: false
}
const getClassesReducer = (state = initialState, actions) => {
    switch (actions.type) {
        case TYPES.GET_CLASSES_REQUEST:
            return {
                ...state,
                loading: true,
                classesData: null,
                isSuccess: false,
                isFailure: false
            }
        case TYPES.GET_CLASSES_SUCCESS:
            console.log('getClasses Reducer===:',actions.user);
            return {
                ...state,
                loading: false,
                classesData: actions.user,
                isSuccess: true,
                isFailure: false
            }
        case TYPES.GET_CLASSES_FAILURE:
            return {
                ...state,
                loading: false,
                classesData: null,
                error: actions.error,
                isSuccess: false,
                isFailure: true
            }
        default:
            return state
    }
}
export default getClassesReducer;
