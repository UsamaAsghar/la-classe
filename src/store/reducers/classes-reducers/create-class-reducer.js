import * as TYPES from '../../actions/types';

const initialState = {
    loading: false,
    classData: null,
    error: null,
    isSuccess: false,
    isFailure: false
}

const createClassReducer = (state = initialState, actions) => {
    switch (actions.type) {
        case TYPES.CREATE_CLASS_REQUEST:
            return {
                ...state,
                loading: true,
                classData: null,
                isSuccess: false,
                isFailure: false
            }
        case TYPES.CREATE_CLASS_SUCCESS:
            console.log('createClass Reducer===:',actions.user);
            return {
                ...state,
                loading: false,
                classData: actions.user,
                isSuccess: true,
                isFailure: false
            }
        case TYPES.CREATE_CLASS_FAILURE:
            return {
                ...state,
                loading: false,
                classData: null,
                error: actions.error,
                isSuccess: false,
                isFailure: true
            }
        default:
            return state
    }
}
export default createClassReducer;
