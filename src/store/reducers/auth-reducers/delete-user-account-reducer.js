import * as TYPES from '../../actions/types';

const initialState = {
    loading: false,
    DeleteUserAccount: null,
    error: null,
    isSuccess: false,
    isFailure: false
}
const DeleteUserAccountReducer = (state = initialState, actions) => {
    switch (actions.type) {
        case TYPES.DELETE_USER_ACCOUNT_REQUEST:
            return {
                ...state,
                loading: true,
                DeleteUserAccount: null,
                isSuccess: false,
                isFailure: false
            }
        case TYPES.DELETE_USER_ACCOUNT_SUCCESS:
            console.log('getParticipantsList Reducer===:',actions.user);
            return {
                ...state,
                loading: false,
                DeleteUserAccount: actions.user,
                isSuccess: true,
                isFailure: false
            }
        case TYPES.DELETE_USER_ACCOUNT_FAILURE:
            return {
                ...state,
                loading: false,
                DeleteUserAccount: null,
                error: actions.error,
                isSuccess: false,
                isFailure: true
            }
        default:
            return state
    }
}
export default DeleteUserAccountReducer;

