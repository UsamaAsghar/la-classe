import * as TYPES from '../../actions/types';

const initialState = {
    loading: false,
    token: null,
    userRegisterRes: null,
    error: null,
    isSuccess: false,
    isFailure: false
}
const signupReducer = (state = initialState, actions) => {
    switch (actions.type) {
        case TYPES.SIGNUP_REQUEST:
            return {
                ...state,
                loading: true,
                userRegisterRes: null,
                isSuccess: false,
                isFailure: false
            }
        case TYPES.SIGNUP_SUCCESS:
            console.log('[signup.js] helper reducer=====:', actions.user);
            return {
                ...state,
                loading: false,
                token: actions.user.token,
                userRegisterRes: actions.user.user,
                isSuccess: true,
                isFailure: false
            }
        case TYPES.SIGNUP_FAILURE:
            return {
                ...state,
                loading: false,
                userRegisterRes: null,
                error: actions.error,
                isSuccess: false,
                isFailure: true
            }
        // case TYPES.SAVE_SIGNUP_REQUEST: 
        //     // console.log('[login.js] helper reducer=====:', actions.user);
        //     return {
        //         ...state,
        //         loading: false,
        //         userRegisterRes: actions.user,
        //         isSuccess: true,
        //         isFailure: false
        //     }
        // case TYPES.LOG_OUT_REQUEST:
        //     return {
        //         ...state,
        //         loading: false,
        //         userRegisterRes: null,
        //         error: null,
        //         isSuccess: false,
        //         isFailure: false
        //     }
        default:
            return state
    }
}
export default signupReducer;
