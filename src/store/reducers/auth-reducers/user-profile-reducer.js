import * as TYPES from '../../actions/types';

const initialState = {
    loading: false,
    userData: null,
    error: null,
    isSuccess: false,
    isFailure: false
}
const userProfileReducer = (state = initialState, actions) => {
    switch (actions.type) {
        case TYPES.USER_FROFILE_REQUEST:
            return {
                ...state,
                loading: true,
                userData: null,
                isSuccess: false,
                isFailure: false
            }
        case TYPES.USER_FROFILE_SUCCESS:
            console.log('userProfile Reducer===:',actions.user);
            return {
                ...state,
                loading: false,
                userData: actions.user,
                isSuccess: true,
                isFailure: false
            }
        case TYPES.USER_FROFILE_FAILURE:
            return {
                ...state,
                loading: false,
                userData: null,
                error: actions.error,
                isSuccess: false,
                isFailure: true
            }
        default:
            return state
    }
}
export default userProfileReducer;
