import * as TYPES from '../../actions/types';

const initialState = {
    loading: false,
    resetData: null,
    error: null,
    isSuccess: false,
    isFailure: false
}
const forgotPasswordReducer = (state = initialState, actions) => {
    switch (actions.type) {
        case TYPES.FORGOT_PASSWORD_REQUEST:
            return {
                ...state,
                loading: true,
                resetData: null,
                isSuccess: false,
                isFailure: false
            }
        case TYPES.FORGOT_PASSWORD_SUCCESS:
            return {
                ...state,
                loading: false,
                resetData: actions.user,
                isSuccess: true,
                isFailure: false
            }
        case TYPES.FORGOT_PASSWORD_FAILURE:
            return {
                ...state,
                loading: false,
                resetData: null,
                error: actions.error,
                isSuccess: false,
                isFailure: true
            }
        default:
            return state
    }
}
export default forgotPasswordReducer;
