import * as TYPES from '../../actions/types';

const initialState = {
    loading: false,
    userData: null,
    error: null,
    isSuccess: false,
    isFailure: false
}
const updateProfileReducer = (state = initialState, actions) => {
    switch (actions.type) {
        case TYPES.UPDATE_FROFILE_REQUEST:
            return {
                ...state,
                loading: true,
                userData: null,
                isSuccess: false,
                isFailure: false
            }
        case TYPES.UPDATE_FROFILE_SUCCESS:
            console.log('getClasses Reducer===:',actions.user);
            return {
                ...state,
                loading: false,
                userData: actions.user,
                isSuccess: true,
                isFailure: false
            }
        case TYPES.UPDATE_FROFILE_FAILURE:
            return {
                ...state,
                loading: false,
                userData: null,
                error: actions.error,
                isSuccess: false,
                isFailure: true
            }
        default:
            return state
    }
}
export default updateProfileReducer;
