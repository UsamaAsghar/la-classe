
import { createStore, applyMiddleware, combineReducers, compose } from 'redux';
import thunk from "redux-thunk";
import { persistStore, persistReducer } from 'redux-persist';
// import storage from 'redux-persist/lib/storage'
import AsyncStorage from '@react-native-community/async-storage';
//****************** Auth Reducers **********************//
import loginReducer from './reducers/auth-reducers/login-reducer';
import signupReducer from './reducers/auth-reducers/signup-reducer';
import forgotPasswordReducer from './reducers/auth-reducers/forgot-password-reducer';
import userProfileReducer from './reducers/auth-reducers/user-profile-reducer';
import updateProfileReducer from './reducers/auth-reducers/update-profile-reducer';
import getClassesReducer from './reducers/classes-reducers/get-classes-reducer';
import createClassReducer from './reducers/classes-reducers/create-class-reducer';
import getcoursesReducer from './reducers/courses-reducers/get-courses-reducer';
import startcoursesReducer from './reducers/courses-reducers/start-course-reducer'
import creatcoursesReducer from './reducers/courses-reducers/create-courses-reducer';
import getnotificationsReducer from './reducers/notifications-reducer/get-notifications-reducer';
import getParticipantslistReducer from './reducers/participants-list-reducer/participants-list-reducer';
import DeleteUserAccountReducer from './reducers/auth-reducers/delete-user-account-reducer';


const rootReducer = combineReducers({
    login: loginReducer,
    signup: signupReducer,
    getCourses: getcoursesReducer,
    getClasses: getClassesReducer,
    userProfile: userProfileReducer,
    startCourse: startcoursesReducer, 
    createClass: createClassReducer,
    creatcourses: creatcoursesReducer,
    updateProfile: updateProfileReducer,
    ForgotPassword: forgotPasswordReducer,
    getnotifications:getnotificationsReducer,
    DeleteUserAccount: DeleteUserAccountReducer,
    getParticipantslist: getParticipantslistReducer,
});

const persistConfig = {
    key: 'root',
    storage: AsyncStorage,
    blacklist: [
        'login',
        'signup',
        'getClasses',
        'getCourses',
        'startCourse',
        'userProfile',
        'createClass',
        'creatcourses',
        'updateProfile',
        'ForgotPassword',
        'getnotifications',
        'getParticipantslist',
        'DeleteUserAccount',
    ]
}

const middleware = applyMiddleware(thunk);


let composeEnhancers = compose;

if (__DEV__) {
    composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
}

const persistedReducer = persistReducer(persistConfig, rootReducer);
export const STORE = createStore(persistedReducer, composeEnhancers(middleware));
export const PERSISTOR = persistStore(STORE);
