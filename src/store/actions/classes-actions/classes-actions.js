import * as TYPES from '../types';
import Api from '../../../services/api'
import { endPoints } from '../../../services'

//=======================================================================================================
//Get All Classes Action
export function getClassesRequest () {
    return {
        type: TYPES.GET_CLASSES_REQUEST,
    }
}

export function getClassesSuccess (user) {
    return {
        type: TYPES.GET_CLASSES_SUCCESS,
        user,
    }
}

export function getClassesFailure (error) {
    return {
        type: TYPES.GET_CLASSES_FAILURE,
        error,
    }
}

export const getClasses = (params) => {
    console.log('token============:', params)
    return async dispatch => {
        dispatch(getClassesRequest())
        try {
            let response = await Api.getAxios(endPoints.classes ,params)
            if (response.success === true && response) {
                console.log('[getClasses-actions] getClasses success case', response);
                dispatch(getClassesSuccess(response))
            } else {
                console.log('[getClasses-action] failure response ', response);
                dispatch(getClassesFailure(response))
            } 
        } catch (error) {
            dispatch(getClassesFailure(error))
        }
    }    
}

//=======================================================================================================
//Create Class Action
export function createClassRequest () {
    return {
        type: TYPES.CREATE_CLASS_REQUEST,
    }
}

export function createClassSuccess (user) {
    return {
        type: TYPES.CREATE_CLASS_SUCCESS,
        user,
    }
}

export function createClassFailure (error) {
    return {
        type: TYPES.CREATE_CLASS_FAILURE,
        error,
    }
}

export const createClass = (params, token) => {
    console.log('createClassAction Params============:', params, token)
    return async dispatch => {
        dispatch(createClassRequest())
        try {
            let response = await Api.postAxios(endPoints.classes ,params, token)
            if (response.success === true && response) {
                console.log('[createClass-actions] success case', response);
                dispatch(createClassSuccess(response))
            } else {
                console.log('[createClass-action] failure response ', response);
                dispatch(createClassFailure(response))
            } 
        } catch (error) {
            dispatch(createClassFailure(error))
        }
    }    
}