import * as TYPES from '../types';
import Api from '../../../services/api'
import { endPoints } from '../../../services'

//=======================================================================================================
//Get All Courses Action
export function getCoursesRequest () {
    return {
        type: TYPES.GET_COURSES_REQUEST,
    }
}

export function getCoursesSuccess (user) {
    return {
        type: TYPES.GET_COURSES_SUCCESS,
        user,
    }
}

export function getCoursesFailure (error) {
    return {
        type: TYPES.GET_COURSES_FAILURE,
        error,
    }
}

export const getCourses = ( token) => {
    console.log('token============:', token)
    return async dispatch => {
        dispatch(getCoursesRequest())
        try {
            let response = await Api.getAxios(endPoints.courses , token)
            console.log('response================:',response);
            if (response.success === true && response) {
                console.log('[getCourses-actions] getCourses success case', response);
                dispatch(getCoursesSuccess(response))
            } else {
                console.log('[getCourses-action] failure response ', response);
                dispatch(getCoursesFailure(response))
            } 
        } catch (error) {
            dispatch(getCoursesFailure(error))
        }
    }    
}

//=======================================================================================================
//Create Courses Action
export function createCoursesRequest () {
    return {
        type: TYPES.CREATE_COURSES_REQUEST,
    }
}

export function createCoursesSuccess (user) {
    return {
        type: TYPES.CREATE_COURSES_SUCCESS,
        user,
    }
}

export function createCoursesFailure (error) {
    return {
        type: TYPES.CREATE_COURSES_FAILURE,
        error,
    }
}

export const createCourses = (params, token) => {
    console.log('createCoursesAction Params============:', params, token)
    return async dispatch => {
        dispatch(createCoursesRequest())
        try {
            let response = await Api.postAxios(endPoints.courses ,params, token)
            if (response.success === true && response) {
                console.log('[createCourses-actions] success case', response);
                dispatch(createCoursesSuccess(response))
            } else {
                console.log('[createCourses-action] failure response ', response);
                dispatch(createCoursesFailure(response))
            } 
        } catch (error) {
            dispatch(createCoursesFailure(error))
        }
    }    
}

//=======================================================================================================
//Start Courses Action
export function startCoursesRequest () {
    return {
        type: TYPES.START_COURSES_REQUEST,
    }
}

export function startCoursesSuccess (user) {
    return {
        type: TYPES.START_COURSES_SUCCESS,
        user,
    }
}

export function startCoursesFailure (error) {
    return {
        type: TYPES.START_COURSES_FAILURE,
        error,
    }
}

export const startCourse = (params, token) => {
    console.log('startCoursesAction Params============:', params, token)
    return async dispatch => {
        dispatch(startCoursesRequest())
        try {
            let response = await Api.postAxios(endPoints.startCourse ,params, token)
            if (response.success === true && response) {
                console.log('[startCourses-actions] success case', response);
                dispatch(startCoursesSuccess(response))
            } else {
                console.log('[startCourses-action] failure response ', response);
                dispatch(startCoursesFailure(response))
            } 
        } catch (error) {
            dispatch(startCoursesFailure(error))
        }
    }    
}