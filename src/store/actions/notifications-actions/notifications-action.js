import * as TYPES from '../types';
import Api from '../../../services/api'
import { endPoints } from '../../../services'

//=======================================================================================================
//Notifications Action
export function getNotificationsRequest () {
    return {
        type: TYPES.GET_NOTIFICATIONS_REQUEST,
    }
}

export function getNotificationsSuccess (user) {
    return {
        type: TYPES.GET_NOTIFICATIONS_SUCCESS,
        user,
    }
}

export function getNotificationsFailure (error) {
    return {
        type: TYPES.GET_NOTIFICATIONS_FAILURE,
        error,
    }
}

export const getNotifications = ( token) => {
    console.log('token============:', token)
    return async dispatch => {
        dispatch(getNotificationsRequest())
        try {
            let response = await Api.getAxios(endPoints.notifications , token)
            console.log('response================:',response);
            if (response.success === true && response) {
                console.log('[getNotifications-actions] getNotifications success case', response);
                dispatch(getNotificationsSuccess(response))
            } else {
                console.log('[getNotifications-action] failure response ', response);
                dispatch(getNotificationsFailure(response))
            } 
        } catch (error) {
            dispatch(getNotificationsFailure(error))
        }
    }    
}