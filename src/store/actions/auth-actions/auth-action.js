import * as TYPES from '../types';
import Api from '../../../services/api'
import { endPoints } from '../../../services'

//Login Action
export function loginRequest () {
    return {
        type: TYPES.USER_LOGIN_REQUEST,
    }
}

export function loginSuccess (user) {
    return {
        type: TYPES.USER_LOGIN_SUCCESS,
        user,
    }
}

export function loginFailure (error) {
    return {
        type: TYPES.USER_LOGIN_FAILURE,
        error,
    }
}

export const loginValidation = (params) => {
    return async dispatch => {
        dispatch(loginRequest())
        try {
            let response = await Api.postAxios(endPoints.login ,params)
            if (response.success === true && response) {
                console.log('[login-actions] login success case', response);
                dispatch(loginSuccess(response))
            } else {
                console.log('[login-action] failure response ', response);
                dispatch(loginFailure(response))
            } 
        } catch (error) {
            dispatch(loginFailure(error))
        }
    }    
}


//=======================================================================================================
//Save Response after signup Action
export function saveSignupRequest (user) {
    return {
        type: TYPES.SAVE_SIGNUP_REQUEST,
        user
    }
}
//signup Action
export function signupRequest () {
    return {
        type: TYPES.SIGNUP_REQUEST,
    }
}

export function signupSuccess (user) {
    return {
        type: TYPES.SIGNUP_SUCCESS,
        user,
    }
}

export function signupFailure (error) {
    return {
        type: TYPES.SIGNUP_FAILURE,
        error,
    }
}

export const signupValidation = (params) => {
    console.log('params receciving ========:', params);
    return async dispatch => {
        dispatch(signupRequest())
        try {
            let response = await Api.postAxios(endPoints.createSignUp ,params)
            if (response.success === true && response) {
                console.log('signup-actions] signup success case', response);
                dispatch(signupSuccess(response))
            } else {
                console.log('[signup-action] failure response ', response);
                dispatch(signupFailure(response))
            } 
        } catch (error) {
            dispatch(signupFailure(error))
        }
    }    
}

export const saveSignupResponse = (data, token) => {
    console.log('[signup response saving]', data);
    return async dispatch => {
        dispatch(loginSuccess({
            user: data,
            token: token
        }))
    }   
}
//=======================================================================================================
//ForgotPassword Action

export function forgotPasswordRequest () {
    return {
        type: TYPES.FORGOT_PASSWORD_REQUEST,
    }
}

export function forgotPasswordSuccess (user) {
    return {
        type: TYPES.FORGOT_PASSWORD_SUCCESS,
        user,
    }
}

export function forgotPasswordFailure (error) {
    return {
        type: TYPES.FORGOT_PASSWORD_FAILURE,
        error,
    }
}

export const forgotPasswordValidation = (params) => {
    return async dispatch => {
        dispatch(forgotPasswordRequest())
        try {
            let response = await Api.postAxios(endPoints.forgotPassword ,params)
            if (response.success === true && response) {
                console.log('[forgotPassword-actions] forgotPassword success case', response);
                dispatch(forgotPasswordSuccess(response))
            } else {
                console.log('[forgotPassword-action] failure response ', response);
                dispatch(forgotPasswordFailure(response))
            } 
        } catch (error) {
            dispatch(forgotPasswordFailure(error))
        }
    }    
}

//=======================================================================================================
//GetProfile Action

export function UserProfileRequest () {
    return {
        type: TYPES.USER_FROFILE_REQUEST,
    }
}

export function UserProfileSuccess (user) {
    return {
        type: TYPES.USER_FROFILE_SUCCESS,
        user,
    }
}

export function UserProfileFailure (error) {
    return {
        type: TYPES.USER_FROFILE_FAILURE,
        error,
    }
}

export const UserProfile = (token) => {
    return async dispatch => {
        dispatch(UserProfileRequest())
        try {
            let response = await Api.getAxios(endPoints.UserProfile,token)
            if (response.success === true && response) {
                console.log('[UserProfile-actions] UserProfile success case', response);
                dispatch(UserProfileSuccess(response.User))
            } else {
                console.log('[UserProfile-action] failure response ', response);
                dispatch(UserProfileFailure(response))
            } 
        } catch (error) {
            dispatch(UserProfileFailure(error))
        }
    }    
}
//=======================================================================================================
//UdateProfile Action

export function UpdateProfileRequest () {
    return {
        type: TYPES.UPDATE_FROFILE_REQUEST,
    }
}

export function UpdateProfileSuccess (user) {
    return {
        type: TYPES.UPDATE_FROFILE_SUCCESS,
        user,
    }
}

export function UpdateProfileFailure (error) {
    return {
        type: TYPES.UPDATE_FROFILE_FAILURE,
        error,
    }
}

export const UpdateProfile = (params, token) => {
    console.log('userProfile Reducer===:',params)
    return async dispatch => {
        dispatch(UpdateProfileRequest())
        try {
            let response = await Api.postAxios(endPoints.UserProfile, params, token)
            if (response.success === true && response) {
                console.log('[UpdateProfile-actions] UpdateProfile success case', response);
                dispatch(UpdateProfileSuccess(response.user))
            } else {
                console.log('[UpdateProfile-action] failure response ', response);
                dispatch(UpdateProfileFailure(response))
            } 
        } catch (error) {
            dispatch(UpdateProfileFailure(error))
        }
    }    
}
//=======================================================================================================
//Delete User Account Action

export function DeleteUserAccountRequest () {
    return {
        type: TYPES.DELETE_USER_ACCOUNT_REQUEST,
    }
}

export function DeleteUserAccountSuccess (user) {
    return {
        type: TYPES.DELETE_USER_ACCOUNT_SUCCESS,
        user,
    }
}

export function DeleteUserAccountFailure (error) {
    return {
        type: TYPES.DELETE_USER_ACCOUNT_FAILURE,
        error,
    }
}

export const DeleteUserAccount = (token) => {
    console.log('DeleteUserAccount Reducer===:')
    return async dispatch => {
        dispatch(DeleteUserAccountRequest())
        try {
            let response = await Api.postAxios(endPoints.deleteUserAccount, token)
            if (response.success === true && response) {
                console.log('[DeleteUserAccount-actions] DeleteUserAccount success case', response);
                dispatch(DeleteUserAccountSuccess(response.user))
            } else {
                console.log('[DeleteUserAccount-action] failure response ', response);
                dispatch(DeleteUserAccountFailure(response))
            } 
        } catch (error) {
            dispatch(DeleteUserAccountFailure(error))
        }
    }    
}

//=======================================================================================================
//Logout Action
export function logOutRequest () {
    return {
        type: TYPES.LOG_OUT_REQUEST,
    }
}

export const logout = () => {
    return async dispatch => {
        dispatch(logOutRequest())
    }   
}

