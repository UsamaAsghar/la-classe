import * as TYPES from '../types';
import Api from '../../../services/api'
import { endPoints } from '../../../services'

//=======================================================================================================
//List of Participents Action
export function getParticipantslistRequest () {
    return {
        type: TYPES.GET_PARTICIPANTSLIST_REQUEST,
    }
}

export function getParticipantslistSuccess (user) {
    return {
        type: TYPES.GET_PARTICIPANTSLIST_SUCCESS,
        user,
    }
}

export function getParticipantslistFailure (error) {
    return {
        type: TYPES.GET_PARTICIPANTSLIST_FAILURE,
        error,
    }
}

export const getParticipantslist = (classID, token) => {
    console.log('token============:', endPoints.participantslist+classID)
    return async dispatch => {
        dispatch(getParticipantslistRequest())
        try {
            let response = await Api.axiosGet(endPoints.participantslist+classID, token)
            console.log('response================:',response);
            if (response.success === true && response) {
                console.log('[getParticipantslist-actions] getParticipantslist success case', response);
                dispatch(getParticipantslistSuccess(response))
            } else {
                console.log('[getParticipantslist-action] failure response ', response);
                dispatch(getParticipantslistFailure(response))
            } 
        } catch (error) {
            dispatch(getParticipantslistFailure(error))
        }
    }    
}