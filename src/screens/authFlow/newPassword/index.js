import React, { Component } from 'react';
import { Image, View, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { styles } from './styles'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { BackgroundWarapper, MediumText, SmallText, InputField, Button } from '../../../components';
import { colors, WP, appImages, loginStrings, newPasswordStrings, forgetPasswordStrings } from '../../../services';
import { withNavigation } from 'react-navigation';

class newPassword extends Component {
    constructor(props) {
        super(props);
        this.state = {
            //states
        }
    }

    render() {
        const { } = this.state;
        return (
            <KeyboardAwareScrollView
                contentContainerStyle={styles.scrollView}
            >
                <BackgroundWarapper isLogin={true}>
                    <View style={styles.lockIconCont}>
                        <Image
                            source={appImages.lockIcon}
                            style={styles.lokIconStyle}
                        />
                    </View>
                    <MediumText
                        text={newPasswordStrings.newPassword}
                        style={styles.newPasswordTextStyle}
                    />
                    <InputField
                        isShowIcon={true}
                        placeholder={newPasswordStrings.placeHolder}
                        image={appImages.lockIcon}
                        styles={styles.PasswordPlaceHolderStyle}
                    />
                    <InputField
                        isShowIcon={true}
                        placeholder={newPasswordStrings.placeHolder}
                        image={appImages.lockIcon}
                        styles={styles.PasswordPlaceHolderStyle}
                    />
                    <Button
                        onPress={() => this.props.navigation.navigate('Login')}
                        title={forgetPasswordStrings.Confirm}
                        titleStyle={{ color: colors.black }}
                        style={styles.signupBtnStyle}
                    />
                </BackgroundWarapper>
            </KeyboardAwareScrollView>
        );
    }
}


mapStateToProps = (state) => {
    return {
        // States
    }
}
mapDispatchToProps = dispatch => {
    return {
        // Actions call
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withNavigation(newPassword));
