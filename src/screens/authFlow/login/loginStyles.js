import { StyleSheet } from 'react-native'
import { family, size, WP, colors } from '../../../services';
export const loginStyles = StyleSheet.create({
    scrollView:{
        flexGrow:1
    },
    signinTextStyle:{
        alignSelf: 'center', 
        marginTop: WP('10'), 
        color: colors.drakBlack, 
        fontWeight: 'bold' 
    },
    subordinateTextStyle:{
        alignSelf: 'center', 
        marginTop: WP('2'), 
        color: colors.mediumGrey 
    },
    signinBtnStyle:{
        alignSelf: 'center', 
        marginTop: WP('5')
    },
    forgetPasswirdTextStyle:{
        alignSelf: 'center', 
        marginTop: WP('2'), 
        color: colors.mediumGrey 
    },
    alreadyAccountTextStyle:{
        alignSelf: 'center', 
        marginTop: WP('4') 
    },
    signupBtnStyle:{
        alignSelf: 'center', 
        marginTop: WP('4'), 
        marginBottom: WP('10'), 
        backgroundColor: colors.btnColorOrange 
    }
    
});