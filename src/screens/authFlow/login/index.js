import React, { Component } from 'react';
import { TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { loginStyles } from './loginStyles'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import Toast from 'react-native-simple-toast'
import { BackgroundWarapper, MediumText, NormalText, InputField, Button } from '../../../components';
import { colors, WP, appImages, loginStrings } from '../../../services';
import { loginValidation } from '../../../store/actions';
import { withNavigation } from 'react-navigation';

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: 'teacher123@gmail.com' ,    //teacher@yahoo.com    //umair3@gmail.com
            password: '123',  //123
            //states
        }
    }
    validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
     }
    componentWillReceiveProps = (props) => {
        console.log('[loginComponent.js] loginRes===:',props);
        const { loginRes } = props
        if (loginRes.isSuccess === true) {
            this.setState({
                email: '',
                password: ''
            })
            this.props.navigation.push('TabStack')
        } else {
            //fail
            if (loginRes.isFailure === true) {
                Toast.show('Please enter valid email or password.')
            }
        }
    }
    login = async() => {
        if (this.state.email == '' || this.validateEmail(this.state.email) == false){
            Toast.show('Please enter your valid email address.')
        } else {
            if (this.state.password == '') {
                Toast.show('Please enter your password.')
            } else {
                let param = {
                    email: this.state.email,
                    password: this.state.password
                }
                await this.props.loginAction(param)
            }
        }
    }
    render() {
        const { email, password } = this.state;
        const { loginRes } = this.props;
        return (
            <KeyboardAwareScrollView
                contentContainerStyle={loginStyles.scrollView}
            >
                <BackgroundWarapper isLogin={true}>
                    <MediumText
                        text={loginStrings.signIn}
                        style={loginStyles.signinTextStyle}
                    />
                    <NormalText
                        text={loginStrings.subTitle}
                        style={loginStyles.subordinateTextStyle}
                    />
                    <InputField
                        value={email}
                        isShowIcon={true}
                        placeholder={loginStrings.emailPlaceHolder}
                        image={appImages.messageIcon}
                        onChangeText={(value)=> this.setState({ email: value })}
                        styles={{marginTop: WP('10')}}
                    />
                    <InputField
                        value={password}
                        isShowIcon={true}
                        placeholder={loginStrings.passwordPlaceHolder}
                        secureTextEntry={true}
                        image={appImages.lockIcon}
                        onChangeText={(value)=> this.setState({ password: value })}
                        styles={{marginTop: WP('5')}}
                    />
                    <Button
                        showLoader={loginRes.loading}
                        title={loginStrings.signIn}
                        style={loginStyles.signinBtnStyle}
                        onPress={() => this.login()}
                    />
                    <TouchableOpacity
                        onPress={() => this.props.navigation.navigate('ForgetPassword')}
                    >
                        <NormalText
                            text={loginStrings.forgetPassword}
                            style={loginStyles.forgetPasswirdTextStyle}
                        />
                    </TouchableOpacity>
                        <NormalText
                            text={loginStrings.noAccount}
                            style={loginStyles.alreadyAccountTextStyle}
                        />
                    <Button
                        title={loginStrings.signUp}
                        titleStyle={{ color: colors.black }}
                        style={loginStyles.signupBtnStyle}
                        onPress={() => this.props.navigation.navigate('SignUp')}
                    />
                </BackgroundWarapper>
            </KeyboardAwareScrollView>
        );
    }
}


mapStateToProps = (state) => {
    return {
        // States
        loginRes: state.login
    }
}
mapDispatchToProps = dispatch => {
    return {
        // Actions call
        loginAction: (params) => dispatch(loginValidation(params))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withNavigation(Login));
