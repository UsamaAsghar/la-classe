import { StyleSheet, Platform } from 'react-native'
import {WP, colors } from '../../../services';
export const styles = StyleSheet.create({
    scrollView: {
        flexGrow: 1
    },
    lockIconCont: {
        height: WP('30'),
        width: WP('30'),
        borderRadius: 100,
        backgroundColor: colors.white,
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center',
        marginVertical: WP('10'),
        ...Platform.select({
            ios:{
                shadowOpacity:0.2
            },
            android:{
                elevation:2
            }
            
        })
    },
    lokIconStyle:{
        height: WP('18'), 
        width: WP('18'), 
        resizeMode: 'contain' 
    },
    forgetPasswordTextStyle: {
        alignSelf: 'center',
        fontSize: WP('6'),
        color: colors.drakBlack,
    },
    subordinateTextStyle: {
        alignSelf: 'center',
        marginTop: WP('5'),
        color: colors.mediumGrey,
        textAlign: 'center',
        marginHorizontal: WP('10')
    },
    signupBtnStyle: {
        alignSelf: 'center',
        marginTop: WP('10'),
        marginBottom: WP('5'),
        backgroundColor: colors.btnColorOrange
    },
    emailPlaceHolderStyle: {
        marginTop: WP('10')
    }
});