import React, { Component } from 'react';
import { Image, View, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { styles } from './styles'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { BackgroundWarapper, MediumText, SmallText, InputField, Button } from '../../../components';
import { colors, WP, appImages, loginStrings, forgetPasswordStrings } from '../../../services';
import { withNavigation } from 'react-navigation';
import { forgotPasswordValidation } from '../../../store/actions';
import Toast from 'react-native-simple-toast'



class ForgetPassword extends Component {
    constructor(props) {
        super(props);
        this.state = {
            //states
            email: ''
        }
    }
    validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
     }
    componentWillReceiveProps = (props) => {
        console.log('[ForgotPasswordComponent.js] ForgotPasswordRes===:',props);
        const { ForgotPasswordRes } = props
        if (ForgotPasswordRes.isSuccess) {
            Toast.show(ForgotPasswordRes.resetData.message)
            this.setState({
                email: '',
            })
            this.props.navigation.push('NewPassword')
        } else {
            //fail
            if (ForgotPasswordRes.isFailure === true) {
                Toast.show('Please enter valid email sddress.')
            }
        }
    }

    ForgotPassword = async () => {
        if (this.state.email == '' || this.validateEmail(this.state.email) == false) {
            Toast.show('Please enter your valid email address.')
        } else {
            let param = {
                email: this.state.email,
            }
            await this.props.ForgotPasswordAction(param)
        }
    }

render() {
    const { email } = this.state;
    const { ForgotPasswordRes } = this.props;

    return (
        <KeyboardAwareScrollView
            contentContainerStyle={styles.scrollView}
        >
            <BackgroundWarapper isLogin={true}>
                <View style={styles.lockIconCont}>
                    <Image
                        source={appImages.lockIcon}
                        style={styles.lokIconStyle}
                    />
                </View>
                <MediumText
                    text={forgetPasswordStrings.forgetPassword}
                    style={styles.forgetPasswordTextStyle}
                />
                <SmallText
                    text={forgetPasswordStrings.subTitleText}
                    style={styles.subordinateTextStyle}
                />
                <InputField
                    value={email}
                    isShowIcon={true}
                    placeholder={loginStrings.emailPlaceHolder}
                    image={appImages.messageIcon}
                    styles={styles.emailPlaceHolderStyle}
                    onChangeText={(value)=> this.setState({ email: value })}
                />
                <Button
                    // onPress={() => this.props.navigation.navigate('NewPassword')}
                    showLoader={ForgotPasswordRes.loading}
                    onPress={() => this.ForgotPassword()}
                    title={forgetPasswordStrings.Confirm}
                    titleStyle={{ color: colors.black }}
                    style={styles.signupBtnStyle}
                />
                <TouchableOpacity
                    onPress={() => this.props.navigation.navigate('Login')}
                >
                    <SmallText
                        text={loginStrings.signIn}
                        style={{ alignSelf: 'center', marginBottom: WP('10'), fontWeight: 'bold', textDecorationLine: 'underline' }}
                    />
                </TouchableOpacity>
            </BackgroundWarapper>
        </KeyboardAwareScrollView>
    );
}
}


mapStateToProps = (state) => {
    return {
        // States
        ForgotPasswordRes: state.ForgotPassword
    }
}
mapDispatchToProps = dispatch => {
    return {
        // Actions call
        ForgotPasswordAction: (params) => dispatch(forgotPasswordValidation(params))
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(withNavigation(ForgetPassword));
