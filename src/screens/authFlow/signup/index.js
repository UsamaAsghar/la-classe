import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity, ScrollView } from 'react-native';
import { styles } from './styles';
import { connect } from 'react-redux'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { BackgroundWarapper, CustomModal, NormalText, InputField, Button } from '../../../components';
import { colors, WP, appImages, registorStrings } from '../../../services';
import { withNavigation } from 'react-navigation';
import { signupValidation, saveSignupResponse } from '../../../store/actions';
import Toast from 'react-native-simple-toast'

class SignUp extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isStudent: true,
            isTeacher: false,
            showModal: false,
            name: '',
            email: '',
            password: '',
            c_password: '',
            establishment: '',
            city: '',
            phonenumber: ''
        }
    }
    validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }
    componentWillReceiveProps = async (props) => {
        console.log('[signupComponent.js] signupRes===:', props);
        const { signupRes, loginRes, saveSignupResAction } = props
        if (signupRes.isSuccess === true) {
            signupRes.isSuccess = false;
            await saveSignupResAction(signupRes.userRegisterRes, signupRes.token)
            this.setState({ 
                name: '',
                email: '',
                password: '',
                c_password: '',
                establishment: '',
                city: '',
                phonenumber: '',
                location:''
            })
            this.setState({
                showModal: true
            }, () => {
                this.props.navigation.push('TabStack')
            })
        } else {
            //fail
            if (signupRes.isFailure === true) {
                Toast.show('Please enter valid Detail.')
            }
        }
    }
    studentSignUp = async () => {
        const { name, email, password, c_password } = this.state;
        if (this.state.name == '') {
            Toast.show("please enter your name.")
        } else {
            if (email == '' || this.validateEmail(email) == false) {
                Toast.show("please enter your valid email address.")
            } else {
                if (password == '') {
                    Toast.show("please enter your password.")
                } else {
                    if (c_password == '') {
                        Toast.show("confirm your password.")
                    } else {
                        if (c_password !== password) {
                            Toast.show("Your passwords are not matching.")
                        } else {
                            let param = {
                                fullName: name,
                                email: email,
                                password: password,
                                isModerator: false
                            }
                            await this.props.signupAction(param)
                        }
                    }
                }
            }
        }
    }
    teacherSignUp = async () => {
        const { name, email, password, c_password, establishment, city, phonenumber } = this.state;
        if (name == '') {
            Toast.show("please enter your name.")
        } else {
            if (establishment == '') {
                Toast.show("please enter your establishment name.")
            } else {
                if (city == '') {
                    Toast.show("please enter the name of your city.")
                } else {
                    if (phonenumber == '') {
                        Toast.show("please enter your phonenumber.")
                    } else {
                        if (email == '' || this.validateEmail(this.state.email) == false) {
                            Toast.show("please enter your valid email address.")
                        } else {
                            if (password == '') {
                                Toast.show("please enter your password.")
                            } else {
                                if (c_password == '') {
                                    Toast.show("confirm your password.")
                                } else {
                                    let param = {
                                        fullName: name,
                                        email: email,
                                        password: password,
                                        etablissement: establishment,
                                        cityName: city,
                                        profileImage: '',
                                        phone: phonenumber,
                                        isModerator: true
                                    }
                                    await this.props.signupAction(param)
                                    // this.setState({ showModal: true })
                                }
                            }
                        }
                    }
                }
            }
        }
    }


    handleFormRendering = (label) => {
        if (label == 'student') {
            this.setState({ isStudent: true, isTeacher: false })
        } else {
            this.setState({ isStudent: false, isTeacher: true })
        }
    }
    toggleModal = () =>
        this.setState({ showModal: !this.state.showModal });

    render() {
        const { name,email,password,c_password,establishment,city,phonenumber, } = this.state;
        const { signupRes } = this.props;
        const { isStudent, isTeacher } = this.state
        return (
            <KeyboardAwareScrollView
                contentContainerStyle={styles.scrollView}
            >
                <BackgroundWarapper isRegister={true}>
                    <ScrollView>
                        <View style={styles.studentTeacherIconView}>
                            <View>
                                <TouchableOpacity
                                    onPress={() => this.handleFormRendering('student')}>
                                    <Image
                                        source={isStudent ? appImages.studentSelectedIcon : appImages.studentUnSelectedIcon}
                                        style={styles.studentIconStyle}
                                    />
                                </TouchableOpacity>
                                <NormalText
                                    text={registorStrings.studentWord}
                                    style={styles.studentIconTextStyle}
                                />
                            </View>
                            <View style={styles.dividerStyle} />
                            <View>
                                <TouchableOpacity
                                    onPress={() => this.handleFormRendering('teacher')}>
                                    <Image
                                        source={isTeacher ? appImages.teacherSelectedIcon : appImages.teacherUnSelectedIcon}
                                        style={styles.teacherIconStyle}
                                    />
                                </TouchableOpacity>
                                <NormalText
                                    text={registorStrings.teacherWord}
                                    style={styles.teacherIconTextStyle}
                                />
                            </View>
                        </View>
                        <InputField
                            value={name}
                            isShowIcon={true}
                            placeholder={registorStrings.namePlaceHolder}
                            image={appImages.profileblackIcon}
                            styles={{ marginTop: WP('10') }}
                            onChangeText={(value) => this.setState({ name: value })}
                        />
                        {
                            isTeacher ?
                                <View>
                                    <InputField
                                        value={establishment}
                                        isShowIcon={true}
                                        placeholder={registorStrings.establishmentPlaceHolder}
                                        image={appImages.instituteIcon}
                                        styles={styles.inputStyle}
                                        onChangeText={(value) => this.setState({ establishment: value })}
                                    />
                                    <InputField
                                        value={city}
                                        isShowIcon={true}
                                        placeholder={registorStrings.locationPlaceHolder}
                                        image={appImages.locationIcon}
                                        styles={styles.inputStyle}
                                        onChangeText={(value) => this.setState({ city: value })}
                                    />
                                    <InputField
                                        value={phonenumber}
                                        isShowIcon={true}
                                        placeholder={registorStrings.phoneNoPlaceHolder}
                                        image={appImages.phoneIcon}
                                        styles={styles.inputStyle}
                                        onChangeText={(value) => this.setState({ phonenumber: value })}
                                    />
                                </View>
                                :
                                null
                        }
                        <InputField
                            value={email}
                            isShowIcon={true}
                            placeholder={registorStrings.emailPlaceHolder}
                            image={appImages.messageIcon}
                            styles={styles.inputStyle}
                            onChangeText={(value) => this.setState({ email: value })}
                        />
                        <InputField
                             value={password}
                            isShowIcon={true}
                            placeholder={registorStrings.passwordPlaceHolder}
                            secureTextEntry={true}
                            image={appImages.lockIcon}
                            styles={styles.inputStyle}
                            onChangeText={(value) => this.setState({ password: value })}
                        />
                        <InputField
                            value={c_password}
                            isShowIcon={true}
                            placeholder={registorStrings.confirmPasswordPlaceHolder}
                            secureTextEntry={true}
                            image={appImages.lockIcon}
                            styles={styles.inputStyle}
                            onChangeText={(value) => this.setState({ c_password: value })}
                        />
                        <Text
                            style={styles.detailTextStyle}
                        >
                            {registorStrings.generalDetail}
                            <Text style={{ color: colors.btnColorOrange }}>{registorStrings.generalCondition}</Text>
                        </Text>
                        <Button
                            showLoader={signupRes.loading}
                            title={registorStrings.creatAccount}
                            titleStyle={{ color: colors.black }}
                            style={styles.creatAccountBtnStyle}
                            onPress={() => {
                                if (isStudent) {
                                    this.studentSignUp()
                                } else {
                                    this.teacherSignUp()
                                }
                            }}
                        />
                        <TouchableOpacity
                            onPress={() => this.props.navigation.goBack()}>
                            <NormalText
                                text={registorStrings.alreadyHaveAccount}
                                style={styles.alreadyAccuontTextStyle}
                            />
                        </TouchableOpacity>
                    </ScrollView>
                    <CustomModal
                        showModal={this.state.showModal}
                        toggleModal={this.toggleModal}
                        message={'You have successfully created your account'}
                    />
                </BackgroundWarapper>
            </KeyboardAwareScrollView>
        );
    }
}


mapStateToProps = (state) => {
    return {
        // States
        loginRes: state.login,
        signupRes: state.signup,
    }
}
mapDispatchToProps = dispatch => {
    return {
        // Actions call
        signupAction: (params) => dispatch(signupValidation(params)),
        saveSignupResAction: (data, token) => dispatch(saveSignupResponse(data, token)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SignUp);
