import { StyleSheet } from 'react-native'
import {  WP, colors } from '../../../services';
export const styles = StyleSheet.create({
    scrollView:{
        flexGrow:1
    },
    container: {
        flex: 1,
        height:"100%",
        width:"100%",
        backgroundColor: colors.bgColor
    },
    studentTeacherIconView:{
        height: WP('40'), 
        width: WP('100'), 
        flexDirection: 'row', 
        alignItems: 'center', 
        justifyContent: 'center', 
        marginTop: WP('3') 
    },
    studentIconStyle:{
        height: WP('30'), 
        width: WP('30'), 
        resizeMode: 'contain' 
    },
    studentIconTextStyle:{
        alignSelf: 'center', 
        marginTop: WP('1'), 
        color: colors.drakBlack
    },
    teacherIconStyle:{
        height: WP('30'), 
        width: WP('30'), 
        resizeMode: 'contain' 
    },
    teacherIconTextStyle:{
        alignSelf: 'center', 
        marginTop: WP('1'), 
        color: colors.drakBlack 
    },
    detailTextStyle:{
        marginTop: WP('10'), 
        textAlign: 'left', 
        marginHorizontal: WP('5'), 
        color: colors.darkGrey, 
        fontSize: WP('3') 
    },
    creatAccountBtnStyle:{
        alignSelf: 'center', 
        marginTop: WP('8'), 
        backgroundColor: colors.btnColorOrange 
    },
    alreadyAccuontTextStyle:{
        alignSelf: 'center', 
        marginTop: WP('3'), 
        color: colors.mediumGrey, 
        marginBottom: WP('10') 
    },
    inputStyle: {
        marginTop: WP('5')
    },
    dividerStyle: { 
        width: WP('10') 
    }
});