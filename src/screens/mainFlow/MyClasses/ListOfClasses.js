import React, { Component } from 'react';
import { Image, View, TouchableOpacity, ScrollView } from 'react-native'
import { styles } from './styles'
import { connect } from 'react-redux';
import { BackgroundWarapper, NormalText, SmallText, MediumText, Button } from '../../../components';
import { colors, appImages, myClassesStrings, ListOfStudentsStrings, WP, myClassesCardStrings } from '../../../services';
import { getClasses } from '../../../store/actions';


class ClassesList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            arr: [{}, {}, {}, {}],
            isClasses: false,
            className: '',
            schoolName: '',
            city: ''
            //states
        }
    }
    componentDidMount = async () => {
        // const { token } = this.props.loginRes;
        // await this.props.getClassesAction(token)
        this.props.getClasses()
    }

    componentWillReceiveProps(props) {
        console.log('[ClassesList.js] ======:', props);
    }

    classCard = (item, key) => {
        return (
            <View style={styles.cardContainer}>
                <View style={styles.cardHeader}>
                    <View style={styles.cardHeaderIconView}>
                        <Image
                            source={appImages.StudentsIcon}
                            style={styles.cardHeaderIconStyle}
                        />
                    </View>
                    <View style={styles.headerTextMainCont}>
                        <NormalText
                            text={item.classeName}
                            style={styles.headerTextStyle}
                        />
                        <TouchableOpacity>
                            <Image
                                source={appImages.MenuIcon}
                                style={styles.menuIconStyle}
                            />
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={styles.cardDetailsCont}>
                    <View style={styles.establishmentDetailView}>
                        <NormalText
                            text={myClassesCardStrings.establishment}
                            style={styles.establishmentTextStyle}
                        />
                        <SmallText
                            text={item.schoolName}
                            style={styles.establishmentNameTextStyle}
                        />
                    </View>
                    <View style={styles.cityDetailView}>
                        <NormalText
                            text={myClassesCardStrings.city}
                            style={styles.cityTexStyle}
                        />
                        <SmallText
                            text={item.city}
                            style={styles.cityNameTextStyle}
                        />
                    </View>
                    <View style={styles.participantDetailView}>
                        <NormalText
                            text={myClassesCardStrings.participants}
                            style={styles.ParticipantsTextStyle}
                        />
                        <SmallText
                            text={myClassesCardStrings.totalParticipants}
                            style={styles.totalParticipantsTextStyle}
                        />
                    </View>
                </View>
                <Button
                    title={myClassesCardStrings.toAccess}
                    titleStyle={{ color: colors.black }}
                    style={styles.cardViewBtnStyle}
                />
            </View>

        )
    }

    render() {
        const { classesData } = this.props.classesData;
        const { isClasses } = this.state;
        return (
            <View style={{ marginBottom: WP('30') }}>
                <MediumText
                    text={myClassesStrings.title}
                    style={styles.titleTextStyle}
                />
                <NormalText
                    text={classesData && classesData.Classes.length + ' Classes'}
                    style={styles.classesDetailTextStyle}
                />
                <ScrollView>
                    {
                        classesData && classesData.Classes.length > 0 ?
                            classesData && classesData.Classes.map((item, key) => {
                                return (this.classCard(item, key))
                            })
                            :
                            <View style={styles.container}>
                                <View style={styles.classPlaceHolderCont}>
                                    <Image
                                        source={appImages.ClassPlaceHolder}
                                        style={styles.classplaceholderImgStyle}
                                    />
                                </View>
                                <MediumText
                                    text={myClassesStrings.noClasses}
                                    style={styles.noClassesTextStyle}
                                />
                                <Button
                                    onPress={() => { this.setState({ isClasses: true }), this.props.onPress() }}
                                    title={myClassesStrings.creatClass}
                                    titleStyle={{ color: colors.black }}
                                    style={styles.creatClasseBtnStyle}
                                />
                            </View>
                    }
                </ScrollView>
            </View>
        );
    }
}
mapStateToProps = (state) => {
    return {
        // States
        loginRes: state.login,
        userPro: state.userProfile,
        UpdateProfile: state.updateProfile,
        classesData: state.getClasses
    }
}
mapDispatchToProps = dispatch => {
    return {
        // Actions call
        getClassesAction: (token) => dispatch(getClasses(token)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ClassesList);