import React, { Component } from 'react';
import { Image, View, TouchableOpacity, ScrollView } from 'react-native'
import { connect } from 'react-redux';
import { styles } from './styles'
import StudentsList from '../ListOfStudents';
import ClassesList from './ListOfClasses'
import ArchivesList from './Archives'
import { BackgroundWarapper, NormalText, SmallText, MediumText, Button } from '../../../components';
import { colors, appImages, myClassesStrings, ListOfStudentsStrings, WP, myClassesCardStrings } from '../../../services';
import { withNavigation } from 'react-navigation';
import { getClasses } from '../../../store/actions';

class MyClasses extends Component {
    constructor(props) {
        super(props);
        this.state = {
            // arr: [{}, {}],
            isClasses: true,
            isStudents: false,
            isArchives: false
        }
    }
    getClasses = async() => {
        const { navigation } = this.props;
        const { token } = this.props.loginRes;
        this.focusListener = navigation.addListener("didFocus", async() => {
            // The screen is focused
            // Call any action
            await this.props.getClassesAction(token)
        });
    }
    tabHandler = (label) => {
        switch (label) {
            case 'std':
                this.setState({
                    isClasses: false,
                    isStudents: true,
                    isArchives: false
                })
                break;
            case 'class':
                this.setState({
                    isClasses: true,
                    isStudents: false,
                    isArchives: false
                })
                break;
            case 'archives':
                this.setState({
                    isClasses: false,
                    isStudents: false,
                    isArchives: true
                })
                break;
            default:
                break;
        }
    }

    render() {
        const { getNotifications, classesData, studentList } = this.props
        const { userProfile } = this.props.loginRes;
        const { isClasses, isStudents, isArchives } = this.state;
        return (
            <BackgroundWarapper
                isProfile={true}
                isLoading={classesData.loading}
                userName={userProfile.fullName}
                email={userProfile.email}
                onPressProfile={() => this.props.navigation.navigate('Profile')}
                notifications={getNotifications.notificationsData ? getNotifications.notificationsData.notifications : null}
            >
                <View style={styles.topBtnsCont}>
                    <TouchableOpacity style={[styles.listOfStudentViewStyle, { backgroundColor: isStudents ? colors.btnColorOrange : '#cccccc' }]}
                        onPress={() => this.tabHandler('std')}
                    >
                        <SmallText
                            text={ListOfStudentsStrings.listOfStudents}
                            style={{ color: isStudents ? colors.white : colors.black }}
                        />
                    </TouchableOpacity>
                    <TouchableOpacity style={[styles.classesViewStyle, { backgroundColor: isClasses ? colors.btnColorOrange : '#ccc' }]}
                        onPress={() => this.tabHandler('class')}
                    >
                        <SmallText
                            text={ListOfStudentsStrings.classes}
                            style={{ color: isClasses ? colors.white : colors.black }}
                        />
                    </TouchableOpacity>
                    <TouchableOpacity style={[styles.archiveCoursesViewStyle, { backgroundColor: isArchives ? colors.btnColorOrange : '#ccc' }]}
                        onPress={() => this.tabHandler('archives')}
                    >
                        <SmallText
                            text={ListOfStudentsStrings.archivedCourses}
                            style={{ color: isArchives ? colors.white : colors.black }}
                        />
                    </TouchableOpacity>
                </View>
                <View style={{ flex: 1 }}>
                    {
                        isClasses ?
                            <ClassesList
                                getClasses={this.getClasses}
                                onPress={() => this.props.navigation.navigate('ClassCreation')} />
                            :
                            isStudents ?
                                <StudentsList />
                                :
                                isArchives ?
                                    <ArchivesList />
                                    :
                                    null
                    }
                </View>
            </BackgroundWarapper>
        );
    }
}


mapStateToProps = (state) => {
    return {
        // States
        loginRes: state.login,
        getNotifications: state.getnotifications,
        classesData: state.getClasses,
        studentList: state.getParticipantslist,
    }
}
mapDispatchToProps = dispatch => {
    return {
        // Actions call
        getClassesAction: (token) => dispatch(getClasses(token)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withNavigation(MyClasses));
