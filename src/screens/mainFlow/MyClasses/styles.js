import { Platform, StyleSheet } from 'react-native'
import { WP, colors } from '../../../services';
import { shadow } from 'react-native-paper';
export const styles = StyleSheet.create({
    scrollView: {
        flexGrow: 1
    },
    ////// top Btns
    topBtnsCont: {
        height: WP('12'),
        width: WP('90'),
        justifyContent: 'space-between',
        alignSelf: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        marginTop: WP('5')
    },
    listOfStudentViewStyle: {
        height: WP('9'),
        width: WP('30'),
        alignItems: 'center',
        backgroundColor: '#ccc',
        borderRadius: 20,
        justifyContent: 'center'
    },
    classesViewStyle: {
        height: WP('9'),
        width: WP('20'),
        alignItems: 'center',
        backgroundColor: '#ccc',
        borderRadius: 20,
        justifyContent: 'center'
    },
    archiveCoursesViewStyle: {
        height: WP('9'),
        width: WP('35'),
        alignItems: 'center',
        backgroundColor: '#ccc',
        justifyContent: 'center',
        borderRadius: 20
    },
    //////
    titleTextStyle: {
        alignSelf: 'center',
        marginTop: WP('5'),
        color: colors.drakBlack,
        fontWeight: 'normal',
        fontSize: WP('6')
    },
    classesDetailTextStyle: {
        alignSelf: 'flex-start',
        marginTop: WP('5'),
        color: colors.Black,
        fontWeight: 'normal',
        marginLeft: WP('6'),
    },
    container: {
        width: WP('90'),
        alignSelf: 'center',
        overflow: 'hidden',
        marginHorizontal: WP('3'),
        backgroundColor: colors.white,
        marginTop: WP('5'),
        // marginBottom: WP('5'),
        borderRadius: 5,
        ...Platform.select({
            ios: {
                shadowColor: '#000',
                shadowOffset: { width: 0, height: 2 },
                shadowOpacity: 0.2,
            },
            android: {
                elevation: 1
            },
        }),
    },
    classplaceholderImgStyle: {
        height: WP('50'),
        width: WP('90'),
        resizeMode: 'contain',
    },
    classPlaceHolderCont: {
        height: WP('50'),
        width: WP('90'),
        alignSelf: 'center',
        backgroundColor: colors.white,
        marginTop: WP('8'),
    },
    noClassesTextStyle: {
        alignSelf: 'center',
        marginTop: WP('15'),
        fontSize: WP('6')

    },
    creatClasseBtnStyle: {
        alignSelf: 'center',
        marginBottom: WP('10'),
        marginTop: WP('5'),
        backgroundColor: colors.btnColorOrange,
        width: WP('80')
    },
    /// Card Style
    cardContainer: {
        width: WP('90'),
        backgroundColor: colors.white,
        alignSelf: 'center',
        borderRadius: WP('2'),
        marginVertical: WP('2'),
        ...Platform.select({
            ios: {
                shadowColor: '#000',
                shadowOffset: { width: 0, height: 2 },
                shadowOpacity: 0.2,
            },
            android: {
                elevation: 1
            },
        }),
    },
    cardHeader: {
        // height: WP('16'),
        width: WP('90'),
        alignItems: 'center',
        flexDirection: 'row',
        borderBottomWidth: WP('0.1'),
        borderBottomColor: colors.lightGrey
    },
    cardHeaderIconView: {
        height: WP('10'),
        width: WP('10'),
        borderRadius: 50,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: colors.btnColorOrange,
        marginHorizontal: WP('3'),
        marginVertical:WP('3')
    },
    cardHeaderIconStyle: {
        height: WP('5'),
        width: WP('8'),
        resizeMode: 'contain',
    },
    headerTextMainCont: {
        width: WP('70'),
        justifyContent: 'space-between',
        flexDirection: 'row',
    },
    headerTextStyle: {
        color: colors.lightBlack,
    },
    menuIconStyle: {
        height: WP('6'),
        width: WP('8'),
        resizeMode: 'contain'
    },
    cardDetailsCont: {
        marginTop: WP('5'),
        width: WP('90'),
        flexDirection: 'row',
    },
    establishmentDetailView: {
        width: WP('30'),
    },
    establishmentTextStyle: {
        color: colors.mediumGrey,
        marginLeft: WP('3')
    },
    establishmentNameTextStyle: {
        color: colors.lightBlack,
        marginHorizontal: WP('3')
    },
    cityDetailView: {
        width: WP('30'),
        alignItems: 'center',
    },
    cityTexStyle: {
        color: colors.mediumGrey,
        marginHorizontal: WP('2')
    },
    cityNameTextStyle: {
        color: colors.lightBlack,
        marginHorizontal: WP('2')
    },
    participantDetailView: {
        width: WP('30'),
    },
    ParticipantsTextStyle: {
        color: colors.mediumGrey,
        marginHorizontal: WP('2')
    },
    totalParticipantsTextStyle: {
        color: colors.lightBlack,
        marginHorizontal: WP('2')
    },
    cardViewBtnStyle: {
        alignSelf: 'center',
        marginBottom: WP('5'),
        marginTop: WP('5'),
        backgroundColor: colors.btnColorOrange,
        width: WP('80'),
        height: WP('10')
    },

    ////////// Archives.js Style
    titleTextStyle: {
        fontWeight: 'normal',
        marginTop: WP('10'),
        fontSize: WP('6'),
        alignSelf: 'center'
    },
    subTitleStyle: {
        fontWeight: 'normal',
        marginTop: WP('6'),
        marginHorizontal: WP('5')
    },
    //Card
    cardContView: {
        width: WP('90'),
        backgroundColor: colors.white,
        alignSelf: 'center',
        borderRadius: WP('2'),
        marginTop: WP('4'),
        alignItems: 'center',
        overflow: 'hidden'
    },
    courseDetailView: {
        height: WP('16'),
        width: WP('90'),
        borderBottomWidth: WP('0.1'),
        borderBottomColor: colors.lightGrey,
        alignItems: 'center',
        flexDirection: 'row'
    },
    capIconView: {
        height: WP('10'),
        width: WP('10'),
        borderRadius: 50,
        backgroundColor: colors.btnColorOrange,
        marginHorizontal: WP('2'),
        alignItems: 'center',
        justifyContent: 'center'
    },
    capIconStyle: {
        height: WP('5'),
        width: WP('6'),
        resizeMode: 'contain'
    },
    courseOneTextStyle: {
        color: colors.black,
        width: WP('70')
    },
    createdByTextStyle: {
        color: colors.lightGrey
    },
    creatorNameTextStyle: {
        color: colors.mediumGrey,
        fontWeight: 'normal'
    },
    menuIconStyle: {
        height: WP('5'),
        color: colors.mediumGrey,
        width: WP('6'),
        resizeMode: 'contain'
    },
    startCourseViewStyle: {
        height: WP('14'),
        width: WP('90'),
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomWidth: WP('0.1'),
        borderBottomColor: colors.lightGrey
    },
    clockIconView: {
        height: WP(6),
        width: WP(7),
        resizeMode: 'contain',
        marginLeft: WP('3')
    },
    startCourseTextStyle: {
        color: colors.lightGrey,
        marginLeft: WP('3')
    },
    documentDetailViewStyle: {
        height: WP('14'),
        width: WP('90'),
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomWidth: WP('0.1'),
        borderBottomColor: colors.lightGrey
    },
    documentIconStyle: {
        height: WP('6'),
        width: WP('7'),
        resizeMode: 'contain',
        marginLeft: WP('3')
    },
    documentDetailTextStyle: {
        color: colors.lightGrey,
        width: WP('70'),
        marginLeft: WP('3')
    },
    achivedTextBtnStyle: {
        height: WP('10'),
        width: WP('80'),
        marginVertical: WP('5'),
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: colors.black,
        borderRadius: 30,
    },
    achivedTextStyle: {
        color: colors.white
    }
});