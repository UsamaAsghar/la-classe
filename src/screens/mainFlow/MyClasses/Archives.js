import React, { Component } from 'react';
import { View, Image, TouchableOpacity, ScrollView } from 'react-native'
import { connect } from 'react-redux';
import { BackgroundWarapper, MediumText, TinyText, NormalText, SmallText } from '../../../components';
import { colors, WP, MyArchivesStrings, appImages } from '../../../services';
import { withNavigation } from 'react-navigation';
import { Colors } from 'react-native/Libraries/NewAppScreen';
import { styles } from './styles';

//Cards
// import GetStartCard from './GetStartCard';
// import StatisticsCard from './StatisticsCard';
// import UpcomingEventsCard from './UpcomingEventsCard';

class ArchivesList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            arr: [{}, {}, {}]
        }
    }

    classCard = (item, key) => {
        return (
            <View style={styles.cardContView}>
                <View style={styles.courseDetailView}>
                    <View style={styles.capIconView}>
                        <Image
                            source={appImages.CapIcon}
                            style={styles.capIconStyle}
                        />
                    </View>
                    <View>
                        <NormalText
                            text={MyArchivesStrings.courseOne}
                            style={styles.courseOneTextStyle}
                        />
                        <View style={{ flexDirection: 'row' }}>
                            <SmallText
                                text={MyArchivesStrings.createdby}
                                style={styles.createdByTextStyle}
                            />
                            <SmallText
                                text={MyArchivesStrings.creatorsname}
                                style={styles.creatorNameTextStyle}
                            />
                        </View>
                    </View>
                    <TouchableOpacity>
                        <Image
                            source={appImages.MenuIcon}
                            style={styles.menuIconStyle}
                        />
                    </TouchableOpacity>
                </View>
                <View style={styles.startCourseViewStyle}>
                    <Image
                        source={appImages.ClockIcon}
                        style={styles.clockIconView}
                    />
                    <SmallText
                        text={MyArchivesStrings.startCourse}
                        style={styles.startCourseTextStyle}
                    />
                </View>
                <View style={styles.documentDetailViewStyle}>
                    <Image
                        source={appImages.DocumentIcon}
                        style={styles.documentIconStyle}
                    />
                    <TinyText
                        text={MyArchivesStrings.documentDetail}
                        style={styles.documentDetailTextStyle}
                    />
                </View>
                <TouchableOpacity style={styles.achivedTextBtnStyle}>
                    <SmallText
                        text={MyArchivesStrings.archived}
                        style={styles.achivedTextStyle}
                    />
                </TouchableOpacity>
            </View>
        )
    }

    render() {
        return (
            <BackgroundWarapper isProfile={false}>
                <ScrollView>
                    <MediumText
                        text={MyArchivesStrings.title}
                        style={styles.titleTextStyle}
                    />
                    <NormalText

                        text={MyArchivesStrings.subtitle}
                        style={styles.subTitleStyle}
                    />
                    {
                        this.state.arr.map((item, key) => {
                            return (this.classCard(item, key))
                        })
                    }
                    <View style={{ marginBottom: WP('5') }}></View>


                </ScrollView>
            </BackgroundWarapper>
        );
    }
}


mapStateToProps = (state) => {
    return {
        // States
    }
}
mapDispatchToProps = dispatch => {
    return {
        // Actions call
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withNavigation(ArchivesList));
