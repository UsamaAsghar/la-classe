import React from 'react'
import { Platform, View, Image, StyleSheet, TouchableOpacity } from 'react-native'
import { TinyText } from '../../../components';
import { colors, WP, HomeScreenStrings, appImages } from '../../../services';
export default GetStartCard = props => {
    return (
        <TouchableOpacity
            disabled={props.disabled}
            style={styles.container}
            onPress={props.onPress}
        >
            <Image
                source={props.image}
                style={styles.propImgStyle}
            />
            <View style={styles.textView}>
                <TinyText
                    text={props.text}
                    style={styles.propTextStyle}
                />
                <Image
                    source={appImages.NextBtnIcon}
                    style={styles.nextBtnStyle}
                />
            </View>
        </TouchableOpacity>
    );
}

const styles = StyleSheet.create({
    container: {
        height: WP('26'), 
        width:WP('43'),
        alignItems:'center',
        backgroundColor: colors.white, 
        borderRadius: WP('2'),
        marginTop:WP('3'),
        // overflow: 'hidden', 
        alignItems: 'center',
        ...Platform.select({
            ios: {
                shadowColor: colors.lightGrey,
                shadowOffset: { width: 0, height: 2 },
                shadowOpacity: 0.5,
            },
            android: {
                elevation: 2
            },
        }),
    },
    propImgStyle:{
        height: WP('8'), 
        width: WP('8'), 
        resizeMode: 'contain', 
        marginVertical: WP('5')
    },
    textView:{
        height: WP('8'), 
        width: WP('43'), 
        backgroundColor: colors.btnColorOrange, 
        flexDirection: 'row', 
        justifyContent: 'space-between',
        alignItems: 'center' 
    },
    propTextStyle:{
        fontWeight: 'normal', 
        marginLeft: WP('3'), 
        fontSize: WP('2.5') 
    },
    nextBtnStyle:{
        height: WP('2'), 
        width: WP('3'), 
        alignSelf: 'center', 
        marginRight: WP('3') 
    }

})

