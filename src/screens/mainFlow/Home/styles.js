import { Platform, StyleSheet } from 'react-native'
import { WP, colors } from '../../../services';
export const styles = StyleSheet.create({
    scrollView: {
        flexGrow: 1
    },
    titleTextStyle:{
        alignSelf: 'center', 
        color: colors.black, 
        marginTop: WP('7'), 
        fontSize: WP('6') 
    },
    subtitleTextStyle:{
        fontWeight: 'normal', 
        alignSelf: 'flex-start', 
        color: colors.Black, 
        marginHorizontal: WP('5'), 
        marginTop: WP('10') 
    },
    getStartedViewStyle:{
        width: WP('90'), 
        flexDirection: 'row', 
        alignSelf: 'center', 
        marginTop: WP('5'), 
        flexWrap: 'wrap',
        justifyContent: 'space-between' ,
        },
    statisticsTextStyle:{
        fontWeight: 'normal', 
        alignSelf: 'flex-start', 
        color: colors.Black, 
        marginHorizontal: WP('5'), 
        marginTop: WP('3') 
    },
    noOfCoursesViewStyle:{
        width: WP('90'), 
        marginTop: WP('3'), 
        backgroundColor: 'transparent', 
        alignSelf: 'center', 
        overflow: 'hidden', 
        justifyContent: 'space-between', 
        flexDirection: 'row', 
        flexWrap: 'wrap'
    },
    upcommingEventTextStyle:{
        width: WP('55'), 
        fontWeight: 'normal', 
        alignSelf: 'flex-start', 
        color: colors.Black, 
        marginHorizontal: WP('5'), 
        marginTop: WP('3') 
    },
    seeMoreTextStyl:{
        height: WP('8'), 
        width: WP('30'), 
        flexDirection: 'row', 
        alignItems: 'center', 
        marginTop: WP('3') 
    },
    seeMoreTextStyle:{
        width: WP('15'), 
        fontWeight: 'normal', 
        marginHorizontal: WP('4'), 
        color: colors.mediumGrey, 
    },
    nextBtnIconStyle:{
        height: WP('2'), 
        color: colors.mediumGrey, 
        width: WP('3'), 
        alignSelf: 'center', 
        resizeMode: 'contain' 
    },
    upcomingEventsCardView:{
        width: WP('90'), 
        justifyContent: 'space-between', 
        alignSelf: 'center', 
        flexDirection: 'row', 
        marginTop: WP('4'), 
        overflow: 'hidden' 
    },
    tutorialTextStyle:{
        fontWeight: 'normal', 
        alignSelf: 'flex-start', 
        color: colors.Black, 
        marginHorizontal: WP('5'), 
        marginTop: WP('3') 
    },
    tutorialVediosView:{
        width: WP('90'), 
        alignSelf: 'center', 
        justifyContent: 'space-between', 
        flexDirection: 'row',
        marginTop:WP('4')
    },
    PlatformVediosView:{
        width: WP('43'), 
        overflow: 'hidden', 
        borderRadius: WP('1.5') 
    },
    platformVediosIconStyle:{
        height: WP('30'), 
        width: WP('43'), 
        resizeMode: 'contain'
    },
    playVediosIconView:{
        height: WP('30'), 
        width: WP('43'), 
        position: 'absolute', 
        justifyContent: 'center' 
    },
    playVediosIconStyle:{
        height: WP('13'), 
        width: WP('13'), 
        resizeMode: 'contain', 
        alignSelf: 'center'
    },
    usageVediosTextView:{
        height: WP('10'), 
        width: WP('43'), 
        alignItems: 'center', 
        backgroundColor: colors.black, 
        justifyContent: 'center' 
    },
    usageVediosTextStyle:{
        fontWeight: 'normal', 
        color: colors.white, 
        fontSize: WP('3'), 
        marginVertical: WP('1') 
    },
    startCourseView:{
        width: WP('43'), 
        overflow: 'hidden', 
        borderRadius: WP('1.5') 
    },
    backgroundIconStyle:{
        height: WP('30'), 
        width: WP('43'), 
        resizeMode: 'contain'
    },
    startCourseTextStyle:{
        fontWeight: 'normal', 
        color: colors.white, 
        fontSize: WP('3'), 
        marginVertical: WP('1') 
    }

});