import React from 'react'
import { Platform, View, Image, StyleSheet, TouchableOpacity } from 'react-native'
import { MediumText, SmallText } from '../../../components';
import { colors, WP, HomeScreenStrings, appImages } from '../../../services';
export default StatisticsCard = props => {
    return (
        <TouchableOpacity
            disabled={props.disabled}
            style={styles.container}
            onPress={props.onPress}
        >
            <SmallText
                text={props.text}
                style={styles.propsTextStyle}
            />
            <View style={styles.numberViewStyle}>
                <MediumText
                    text={props.value}
                    style={styles.numberTextStyle}
                />
            </View>
        </TouchableOpacity>
    );
}

const styles = StyleSheet.create({
    container: {
        height: WP('15'),
        marginBottom: WP('2'),
        // marginHorizontal: WP('1'),
        backgroundColor: colors.white, 
        borderRadius: WP('1.5'), 
        alignItems: 'center', 
        // overflow: 'hidden', 
        flexDirection: 'row',
        ...Platform.select({
            ios: {
                shadowColor: colors.lightGrey,
                shadowOffset: { width: 0, height: 2 },
                shadowOpacity: 0.5,
            },
            android: {
                elevation: 2
            },
        }),
    },
    propsTextStyle:{
        width: WP('25'), 
        marginHorizontal: WP('2'),
        fontSize:WP('3') 
    },
    numberViewStyle:{
        height: WP('14'), 
        width: WP('14'),
        marginHorizontal: 2, 
        backgroundColor: colors.c_bgColor, 
        alignItems: 'center',
        justifyContent: 'center', 
        overflow: 'hidden', 
        shadowOpacity: 0.2
    },
    numberTextStyle:{
        fontSize: WP('6'), 
        color: colors.btnColorOrange 
    }

})

