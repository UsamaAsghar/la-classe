import React from 'react'
import { Platform, View, Image, StyleSheet, TouchableOpacity } from 'react-native'
import { MediumText, SmallText, NormalText } from '../../../components';
import { colors, WP, HomeScreenStrings, appImages } from '../../../services';
export default UpcomingEventsCard = props => {


    this.state = {
        isClasses: false
    }

    return (
        <View style={styles.container}>
            <View style={styles.eventNameTextView}>
                <NormalText
                    text={HomeScreenStrings.eventName}
                    style={styles.eventNameTextStyle}
                />
                <Image
                    source={appImages.StarIcon}
                    style={styles.starIconStyle}
                />
            </View>
            <View style={{ flexDirection: 'row' }}>
                <View style={styles.classCardView}>
                    <TouchableOpacity>
                        <SmallText
                            text={HomeScreenStrings.month}
                            style={styles.monthNameStyle}
                        />
                    </TouchableOpacity>
                    <Image
                        source={appImages.WhiteLineIcon}
                        style={styles.whiteLineIconStyle}
                    />
                    <TouchableOpacity>
                        <MediumText
                            text={HomeScreenStrings.date}
                            style={styles.dateTextStyle}
                        />
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <SmallText
                            text={HomeScreenStrings.day}
                            style={styles.dayTextStyle}
                        />
                    </TouchableOpacity>
                </View>
                <View style={styles.classCardView}>
                    <TouchableOpacity>
                        <SmallText
                            text={HomeScreenStrings.time}
                            style={styles.monthNameStyle}
                        />
                    </TouchableOpacity>
                    <Image
                        source={appImages.WhiteLineIcon}
                        style={styles.whiteLineIconStyle}
                    />
                    <TouchableOpacity>
                        <MediumText
                            text={HomeScreenStrings.hour}
                            style={styles.dateTextStyle}
                        />
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <SmallText
                            text={HomeScreenStrings.timeDimention}
                            style={styles.dayTextStyle}
                        />
                    </TouchableOpacity>
                </View>
            </View>
            <View style={styles.studentRingIconView}>
                <Image
                    source={appImages.StudentRingIcon}
                    style={styles.studentRingIconStyle}
                />
                <View style={styles.dateTextView}>
                    <SmallText
                        text={HomeScreenStrings.date}
                        style={styles.dateText}
                    />
                </View>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        width: WP('43'),
        borderRadius: WP('1.5'),
        backgroundColor: colors.white,
        borderRadius: WP('1.5'),
        marginHorizontal: 2,
        marginVertical: WP('1'),
        // overflow: 'hidden',
        ...Platform.select({
            ios: {
                shadowColor: colors.lightGrey,
                shadowOffset: { width: 0, height: 2 },
                shadowOpacity: 0.5,
            },
            android: {
                elevation: 2
            },
        }),
    },
    eventNameTextView: {
        height: WP('10'),
        width: WP('40'),
        flexDirection: 'row',
        backgroundColor: colors.white,
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    eventNameTextStyle: {
        fontWeight: 'normal',
        marginLeft: WP('2'),
        color: colors.black,
    },
    starIconStyle: {
        height: WP('5'),
        marginRight: WP('2'),
        resizeMode: 'contain',
    },
    //ClassCard
    classCardView: {
        height: WP('22'),
        width: WP('17'),
        marginTop: WP('2'),
        backgroundColor: colors.c_bgColor,
        borderRadius: WP('1.5'),
        marginHorizontal: WP('2'),
        alignItems: "center"
    },
    monthNameStyle: {
        fontWeight: 'normal',
        marginVertical: WP('1')
    },
    whiteLineIconStyle: {
        height: WP('0.5'),
        width: WP('15'),
        resizeMode: 'contain'
    },
    dateTextStyle: {
        fontWeight: 'normal',
        fontSize: WP('4'),
        color: colors.btnColorOrange,
        marginVertical: WP('2.5')
    },
    dayTextStyle: {
        fontWeight: 'normal'
    },
    studentRingIconView: {
        height: WP('12'),
        width: WP('35'),
        marginTop: WP('2'),
        alignItems: 'center',
        flexDirection: 'row'
    },
    studentRingIconStyle: {
        height: WP('7'),
        resizeMode: 'contain',
        color: colors.mediumGrey,
        width: WP('20'),
        marginHorizontal: WP('2')
    },
    dateTextView: {
        height: WP('6'),
        width: WP('6'),
        borderRadius: 50,
        alignItems: 'center',
        borderWidth: WP('0.1'),
        backgroundColor: colors.c_bgColor,
        borderColor: 'orange',
        marginLeft: WP('-4')
    },
    dateText: {
        fontWeight: 'normal',
        marginVertical: WP('1')
    }


})

