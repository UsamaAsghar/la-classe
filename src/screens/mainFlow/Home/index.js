import React, { Component } from 'react';
import { View, Image, ImageBackground, TouchableOpacity, ScrollView } from 'react-native'
import { connect } from 'react-redux';
import { BackgroundWarapper, MediumText, NormalText, SmallText } from '../../../components';
import { colors, WP, HomeScreenStrings, appImages } from '../../../services';
import { withNavigation } from 'react-navigation';
import { styles } from './styles'
//Cards
import GetStartCard from './GetStartCard';
import StatisticsCard from './StatisticsCard';
import UpcomingEventsCard from './UpcomingEventsCard';
import { getNotifications, } from '../../../store/actions';


class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            //states
            message:''
        }
    }
    componentDidMount = async () => {
        const { token } = this.props.loginRes;
        console.log('token-------:', token);
        await this.props.getNotificationsAction(token)
    }

    componentWillReceiveProps(props) {
        console.log('[getnotifications.js] ======:', props);
    }

    render() {
        const  { userProfile } = this.props.loginRes;
        const { getNotifications } = this.props
        return (
            <BackgroundWarapper 
                isProfile={true}
                notifications={getNotifications.notificationsData ? getNotifications.notificationsData.notifications : null}
                userName={userProfile.fullName} 
                email={userProfile.email}
                onPressProfile={() => this.props.navigation.navigate('Profile')}
            >
                <ScrollView>
                    <MediumText
                        text={HomeScreenStrings.title}
                        style={styles.titleTextStyle}
                    />
                    <NormalText
                        text={HomeScreenStrings.subTitle}
                        style={styles.subtitleTextStyle}
                    />
                    <View style={styles.getStartedViewStyle}>
                        <GetStartCard
                            image={appImages.StudentsLightIcon}
                            text={HomeScreenStrings.creatClass}
                            onPress={()=> this.props.navigation.push('ClassCreation')}
                        />
                        <GetStartCard
                            image={appImages.LessonIcon}
                            text={HomeScreenStrings.creatLesson}
                            onPress={() => this.props.navigation.push('CourseCreation')}
                        />
                        {/* <GetStartCard
                            image={appImages.CopyIcon}
                            text={HomeScreenStrings.creatClass}
                        />
                        <GetStartCard
                            image={appImages.unselectedCalenderIcon}
                            text={HomeScreenStrings.creatClass}
                        /> */}
                    </View>
                    <NormalText
                        text={HomeScreenStrings.statistics}
                        style={styles.statisticsTextStyle}
                    />
                    <View style={styles.noOfCoursesViewStyle}>
                        <StatisticsCard
                        onPress={() => this.props.navigation.navigate('MyCourses')}
                            value={HomeScreenStrings.noOfCourses}
                            text={HomeScreenStrings.totalNoOfStudents}
                        />
                        <StatisticsCard
                            value={HomeScreenStrings.noOfClasses}
                            text={HomeScreenStrings.totalNoOfClasses}
                            onPress={() => this.props.navigation.navigate('MyClasses')}
                        />
                        <StatisticsCard
                            value={HomeScreenStrings.noOfMyStudents}
                            text={HomeScreenStrings.totalNoOfMyStudents}
                        />
                        <StatisticsCard
                            value={HomeScreenStrings.noOfnextCourses}
                            text={HomeScreenStrings.nextCourse}
                        />
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                        <NormalText
                            text={HomeScreenStrings.upcommingEvent}
                            style={styles.upcommingEventTextStyle}
                        />
                        <TouchableOpacity style={styles.seeMoreTextStyl}
                            onPress={() => this.props.navigation.navigate('Calender')}
                        >
                            <SmallText
                                text={HomeScreenStrings.seemore}
                                style={styles.seeMoreTextStyle}
                            />
                            <Image
                                source={appImages.NextBtnIcon}
                                style={styles.nextBtnIconStyle}
                            />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.upcomingEventsCardView}>
                        <UpcomingEventsCard
                        />
                        <UpcomingEventsCard
                        />
                    </View>
                    <NormalText
                        text={HomeScreenStrings.tutorials}
                        style={styles.tutorialTextStyle}
                    />
                    <View style={styles.tutorialVediosView}>
                        <View style={styles.PlatformVediosView}>
                            <ImageBackground
                                source={appImages.PaltformVediosIcon}
                                style={styles.platformVediosIconStyle}
                            />
                            <TouchableOpacity style={styles.playVediosIconView}>
                                <Image
                                    source={appImages.PlayVedioSelectedIcon}
                                    style={styles.playVediosIconStyle}
                                />
                            </TouchableOpacity>
                            <View style={styles.usageVediosTextView}>
                                <SmallText
                                    text={HomeScreenStrings.usage}
                                    style={styles.usageVediosTextStyle}
                                />
                            </View>
                        </View>
                        <View style={styles.startCourseView}>
                            <ImageBackground
                                source={appImages.CourseVediosIcon}
                                style={styles.backgroundIconStyle}
                            />
                            <TouchableOpacity style={styles.playVediosIconView}>
                                <Image
                                    source={appImages.PlayVedioSelectedIcon}
                                    style={styles.playVediosIconStyle}
                                />
                            </TouchableOpacity>
                            <View style={styles.usageVediosTextView}>
                                <SmallText
                                    text={HomeScreenStrings.startCourse}
                                    style={styles.startCourseTextStyle}
                                />
                            </View>
                        </View>
                    </View>

                    <View style={{ marginBottom: WP('10') }}></View>
                </ScrollView>
            </BackgroundWarapper>
        );
    }
}



mapStateToProps = (state) => {
    return {
        loginRes: state.login,
        coursesData: state.getCourses,
        getNotifications: state.getnotifications,
    }
}
mapDispatchToProps = dispatch => {
    return {
        // Actions call
        getNotificationsAction: (token) => dispatch(getNotifications(token)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withNavigation(Home));
