import { StyleSheet } from 'react-native'
import { family, size, WP, colors } from '../../../services';
export const styles = StyleSheet.create({
    scrollView: {
        flexGrow: 1
    },
    profileIconStyle: {
        height: WP('40'),
        width: WP('40'),
        resizeMode: 'contain',
        alignSelf: 'center',
        marginTop: WP('5')
    },
    titleTextStyle: {
        alignSelf: 'center',
        marginTop: WP('10'),
        color: colors.drakBlack,
        fontWeight: 'normal'
    },
    nameTextStyle: {
        alignSelf: 'center',
        color: colors.drakBlack
    },
    emailTextStyle: {
        alignSelf: 'center',
        color: colors.mediumGrey,
        
    },
    addressTextStyle: {
        alignSelf: 'center',
        color: colors.mediumGrey
    },
    changePasswordTextStyle: {
        marginHorizontal: WP("6"),
        color: colors.lightBlack,
        marginVertical: WP("3")
    },
    textInputContView:{
        height: WP('13'), 
        width: WP('90'), 
        flexDirection: 'row', 
        alignItems: 'center', 
        justifyContent: 'space-between', 
        backgroundColor: colors.white, 
        borderRadius:WP('1.5'),
        marginTop:WP('5'),
        alignSelf: 'center', 
        ...Platform.select({
            ios: {
                shadowColor: colors.lightGrey,
                shadowOffset: { width: 0, height: 2 },
                shadowOpacity: 0.5,
            },
            android: {
                elevation: 2
            },
        }),
    },
    placeholderContStyle:{
        height: WP('10'), 
        marginLeft: WP('5'), 
        backgroundColor: colors.white, 
        width: WP('60') 
    },
    deleteIconContStyle:{
        height:WP('12'),
        width:WP('12'),
        backgroundColor:colors.c_bgColor,
        marginRight:WP('2'),
        justifyContent:'center'
    },
    searchIconStyle:{
        height: WP('7'), 
        width: WP('7'), 
        alignSelf: 'center' ,
        backgroundColor:colors.c_bgColor,
        alignSelf:'center'
    },
    confirmBtnStyle: {
        alignSelf: 'center',
        marginTop: WP('5'),
        marginBottom: WP('10'),
        backgroundColor: colors.btnColorOrange,
    },
    YesBtnStyle: {
        height:WP('7'),
        width:WP('30'),
        alignSelf: 'center',
        marginTop: WP('5'),
        marginBottom: WP('10'),
        marginLeft:WP('5'),
        backgroundColor: colors.btnColorOrange,
    },
    NoBtnStyle: {
        height:WP('7'),
        width:WP('30'),
        alignSelf: 'center',
        marginTop: WP('5'),
        marginBottom: WP('10'),
        marginRight:WP('5'),
        backgroundColor: colors.btnColorOrange,
    },
});