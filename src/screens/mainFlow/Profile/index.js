import React, { Component } from 'react';
import { connect } from 'react-redux';
import { styles } from './styles';
import Modal from 'react-native-modal';
import { Image, View, TextInput, TouchableOpacity } from 'react-native'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { BackgroundWarapper, SmallText, NormalText, InputField, Button } from '../../../components';
import { colors, WP, appImages, profileStrings } from '../../../services';
import { withNavigation } from 'react-navigation';
import { UserProfile, UpdateProfile, saveSignupResponse } from '../../../store/actions';
import Toast from 'react-native-simple-toast'




class profile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showModal: false,
            name: '',
            email: '',
            phone_no: '',
            city: '',
            password: '',
            etablissement: '',
            isModerator: false,
            image: ''
        }
    }
    componentDidMount = async () => {
        const { token } = this.props.loginRes;
        console.log('token-------:', token);
        await this.props.userProfileAction(token)
    }
    componentWillReceiveProps = async (props) => {
        console.log('[profile.js]componentWillReceiveProps ===:', props);
        const { userProfile } = props.loginRes
        if (props.userPro.isSuccess) {
            props.userPro.isSuccess = false
            if (userProfile.isModerator) {
                this.setState({
                    isModerator: userProfile.isModerator,
                    name: userProfile.fullName,
                    email: userProfile.email,
                    city: userProfile.cityName,
                    phone_no: userProfile.phone,
                    etablissement: userProfile.etablissement,
                    image: userProfile.profileImage,
                })
            } else {
                this.setState({
                    isModerator: userProfile.isModerator,
                    name: userProfile.fullName,
                    email: userProfile.email,
                })
            }
        }
        //UpdateProfile Setting
        const { UpdateProfile, saveSignupResAction, loginRes } = props
        if (props.UpdateProfile.isSuccess) {
            props.UpdateProfile.isSuccess = false
            await saveSignupResAction(props.UpdateProfile.userData, loginRes.token)
            if (UpdateProfile.userData.isModerator) {
                this.setState({
                    isModerator: UpdateProfile.userData.isModerator,
                    name: UpdateProfile.userData.fullName,
                    email: UpdateProfile.userData.email,
                    city: UpdateProfile.userData.cityName,
                    phone_no: UpdateProfile.userData.phone,
                    etablissement: UpdateProfile.userData.etablissement,
                    image: UpdateProfile.userData.profileImage,
                })
            } else {
                this.setState({
                    isModerator: UpdateProfile.userData.isModerator,
                    name: UpdateProfile.userData.fullName,
                    email: UpdateProfile.userData.email,
                })
            }
        }
    }
    validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }
    UpdateProfile = async () => {
        const { token } = this.props.loginRes;
        const { email, name, phone_no, password, city, isModerator } = this.state
        if (email == '' || this.validateEmail(email) == false) {
            Toast.show('Please enter your valid email address.')
        } else {
            if (this.state.name == '') {
                Toast.show('Please enter your name.')
            } else {
                if (isModerator == true) {
                    var param = {
                        fullName: name,
                        email: email,
                        cityName: this.state.city,
                        phone: this.state.phone_no,
                        isModerator: isModerator,
                        etablissement: "etablissement",
                        // password: "password",
                        // profileImage: "image.png",
                    }
                } else {
                    var param = {
                        fullName: name,
                        email: email,
                        isModerator: isModerator,
                        // password: "password",
                        // profileImage: "image.png",
                    }
                }
                await this.props.UpdateProfileAction(param, token)
            }
        }
    }
    toggleModal = () =>
        this.setState({ showModal: !this.state.showModal })


    render() {
        const { email, password, name, phone_no, c_password, establishment, isModerator, city } = this.state;
        const { loginRes, userPro, UpdateProfile } = this.props;
        const { userProfile } = this.props.loginRes;
        const { getNotifications } = this.props
        const { isStudentList, showModal } = this.state;

        return (
            <KeyboardAwareScrollView
                contentContainerStyle={styles.scrollView}
            >
                <BackgroundWarapper
                    isLoading={userPro.loading}
                    isBackBtn={true}
                    onBackBtn={()=> this.props.navigation.goBack()}
                    isProfile={true}
                    userName={userProfile.fullName}
                    email={userProfile.email}
                    onPressProfile={() => this.props.navigation.navigate('Profile')}
                    notifications={getNotifications.notificationsData ? getNotifications.notificationsData.notifications : null}
                >
                    <NormalText
                        text={profileStrings.header}
                        style={styles.titleTextStyle}
                    />
                    <Image
                        source={appImages.profileIcon}
                        style={styles.profileIconStyle}
                    />
                    <NormalText
                        text={name}
                        style={styles.nameTextStyle}
                    />
                    <SmallText
                        text={email}
                        style={styles.emailTextStyle}
                    />
                    {
                        isModerator ?
                            <SmallText
                                text={city}
                                style={styles.addressTextStyle}
                            />
                            : null
                    }
                    <InputField
                        value={name}
                        isShowIcon={false}
                        placeholder={profileStrings.name}
                        styles={{ marginTop: WP('10'), }}
                        onChangeText={(value) => this.setState({ name: value })}
                    />
                    <InputField
                        value={email}
                        isShowIcon={false}
                        placeholder={profileStrings.email}
                        styles={{ marginTop: WP('5') }}
                        onChangeText={(value) => this.setState({ email: value })}
                    />
                    {
                        isModerator ?
                            <View>
                                <InputField
                                    value={phone_no}
                                    isShowIcon={false}
                                    placeholder={profileStrings.address}
                                    styles={{ marginTop: WP('5') }}
                                    onChangeText={(value) => this.setState({ phone_no: value })}
                                />
                                <InputField
                                    value={city}
                                    isShowIcon={false}
                                    placeholder={profileStrings.state}
                                    styles={{ marginTop: WP('5') }}
                                    onChangeText={(value) => this.setState({ city: value })}
                                />
                            </View>
                            : null
                    }
                    <SmallText
                        text={profileStrings.changePassword}
                        style={styles.changePasswordTextStyle}
                    />
                    <InputField
                        value={password}
                        isShowIcon={false}
                        placeholder={profileStrings.changePassword}
                        styles={{ marginTop: WP('5') }}
                        onChangeText={(value) => this.setState({})}
                    />
                    <InputField
                        value={password}
                        isShowIcon={false}
                        placeholder={profileStrings.changePassword}
                        styles={{ marginTop: WP('5') }}
                        onChangeText={(value) => this.setState({})}
                    />
                    <SmallText
                        text={profileStrings.deleteAccount}
                        style={styles.changePasswordTextStyle}
                    />
                    <View style={styles.textInputContView}>
                        <TextInput
                            value={email}
                            keyboardType='default'
                            placeholderTextColor='Grey'
                            disableFullscreenUI={true}
                            style={styles.placeholderContStyle}
                        />
                        <TouchableOpacity style={styles.deleteIconContStyle}
                            onPress={() => this.setState({ showModal: true })}
                        >
                            <Image
                                source={appImages.DeleteIcon}
                                style={styles.searchIconStyle}
                            />
                        </TouchableOpacity>
                    </View>
                    <Button
                        showLoader={UpdateProfile.loading}
                        title={profileStrings.Save}
                        titleStyle={{ color: colors.black }}
                        style={styles.confirmBtnStyle}
                        onPress={() => {
                            this.UpdateProfile()
                        }}
                    />

                    <Modal
                        animationInTiming={200}
                        animationOutTiming={100}
                        animationIn="slideInLeft"
                        animationOut="slideOutRight"
                        avoidKeyboard={true}
                        transparent={true}
                        isVisible={showModal}
                        onBackdropPress={this.toggleModal}
                        style={{ flex: 1, justifyContent: 'center' }}
                    >
                        <View style={{ width: WP('80'), alignSelf: 'center', backgroundColor: colors.white, borderRadius: WP('5'), overflow: 'hidden' }}>
                            <View style={{ height: WP('15'), width: WP('80'), marginVertical: WP('5'), justifyContent: 'center', alignItems: 'center' }}>
                                <SmallText
                                    text={profileStrings.deleteAccount}
                                    style={{ fontSize: WP('5'), color: colors.lightBlack, textAlign: 'center', marginHorizontal:WP('4') }}
                                />
                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                <Button
                                    showLoader={UpdateProfile.loading}
                                    title={profileStrings.Yes}
                                    titleStyle={{ color: colors.black }}
                                    style={styles.YesBtnStyle}
                                    onPress={() => {
                                        Toast.show('your account has successfully been deleted')
                                    }}
                                />
                                <Button
                                    showLoader={UpdateProfile.loading}
                                    onPress={() => this.props.navigation.goBack()}
                                    title={profileStrings.No}
                                    titleStyle={{ color: colors.black }}
                                    style={styles.NoBtnStyle}

                                />
                            </View>
                        </View>
                    </Modal>

                </BackgroundWarapper>
            </KeyboardAwareScrollView>
        );
    }
}


mapStateToProps = (state) => {
    return {
        // States
        loginRes: state.login,
        userPro: state.userProfile,
        UpdateProfile: state.updateProfile,
        getNotifications: state.getnotifications,
    }
}
mapDispatchToProps = dispatch => {
    return {
        // Actions call
        userProfileAction: (token) => dispatch(UserProfile(token)),
        UpdateProfileAction: (params, token) => dispatch(UpdateProfile(params, token)),
        saveSignupResAction: (data, token) => dispatch(saveSignupResponse(data, token)),

    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withNavigation(profile));
