import React, { Component } from 'react';
import { connect } from 'react-redux';
import { styles } from './styles'
import { TextInput, ActivityIndicator } from 'react-native'
import {  TinyText, MediumText, SmallText, Button } from '../../../components';
import { colors, WP, appImages, ListOfStudentsStrings, HP } from '../../../services';
import { withNavigation } from 'react-navigation';
import { Image, View, ScrollView, TouchableOpacity, } from 'react-native';
import Modal from 'react-native-modal';
import Toast from 'react-native-simple-toast';
import { Avatar } from 'react-native-elements';
import { getParticipantslist } from '../../../store/actions';

class StudentsList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            arr: [{}, {}, {}, {}, {}, {}],
            showModal: false,
        }
    }

    componentDidMount = async () => {
        const { getParticipantslistAction } = this.props;
        const { token } = this.props.loginRes;
        let classID = '5ed3ab3f93f72f24a6473bc3';
        await getParticipantslistAction(classID,token)
    }
    componentWillReceiveProps (nextProps) {
        console.log('[ListOfStudents.js] componentWillReceiveProps==---------:',nextProps);
        const { studentList } = nextProps;
        if (studentList.isSuccess) {
            nextProps.studentList.isSuccess = false;
            Toast.show('Student list fetched successfully')
        } else {
            if (studentList.isFailure) {
                Toast.show('There must be an issue, please try again.')
            }
        }
    }

    toggleModal = () =>
        this.setState({ showModal: !this.state.showModal })

    classCard = (item, key) => {
        return (
            <View style={styles.cardChildView}>
                <Avatar
                    // onPress={props.onPressProfile}
                    rounded
                    size="medium"
                    source={appImages.userProfileIcon}
                    containerStyle={{ marginHorizontal: WP('3') }}
                />
                <View style={styles.textViewStyle}>
                    <SmallText
                        text={ListOfStudentsStrings.name}
                        style={styles.nameTextStyle}
                    />
                    <TinyText
                        text={ListOfStudentsStrings.email}
                        style={styles.emailTextStyle}
                    />
                </View>
                <TouchableOpacity style={styles.clockIconViewStyle}>
                    <Image
                        source={appImages.ClockIcon}
                        style={styles.clockIconStyle}
                    />
                </TouchableOpacity>
                <TouchableOpacity style={styles.deleteIconViewStyle}
                    onPress={() => this.setState({ showModal: true })}
                >
                    <Image
                        source={appImages.DeleteIcon}
                        style={styles.deleteIconStyle}
                    />
                </TouchableOpacity>

            </View>

        )
    }

    toggleModal = () =>
        this.setState({ showModal: !this.state.showModal })


    render() {
        const { isStudentList, showModal } = this.state;
        const { studentList } = this.props;
        return (
            <View style={{ flex: 1 }}>
                <MediumText
                    text={ListOfStudentsStrings.title}
                    style={styles.titleTextStyle}
                />
                <View style={styles.ButtonsMainCont}>
                <Button
                        isRightShowIcon={true}
                        title={ListOfStudentsStrings.export}
                        titleStyle={{ color: colors.white }}
                        titleContainerStyle={{ width: WP('15') }}
                        style={styles.ExportBtnStyle}
                    />
                <Button
                        isRightShowIcon={true}
                        image={appImages.AddIcon}
                        title={ListOfStudentsStrings.add}
                        titleStyle={{ color: colors.white, marginLeft:WP('-3') }}
                        titleContainerStyle={{ width: WP('8') }}
                        style={styles.startCardBtnStyle}
                        onPress={() => this.props.navigation.navigate('AddStudents')}
                    />
                </View>
                <View>
                    <View style={styles.textInputContView}>
                        <TextInput
                            placeholder='Find a Friend'
                            keyboardType='default'
                            placeholderTextColor='lightGrey'
                            style={styles.placeholderContStyle}
                        />
                        <TouchableOpacity>
                            <Image
                                source={appImages.SearchIcon}
                                style={styles.searchIconStyle}
                            />
                        </TouchableOpacity>
                    </View>
                </View>
                <ScrollView>
                    {
                        studentList.participantslistData ?
                            <View style={styles.cardCont}>

                                {
                                    studentList.participantslistData.success && studentList.participantslistData.Participants.length > 0 ? 
                                        studentList.participantslistData.Participants.map((item, key) => {
                                            return (this.classCard(item, key))
                                        })
                                        :
                                        <MediumText
                                            text={'No student found'}
                                            style={{ fontSize: WP('4'), color: colors.mediumGrey, alignSelf: 'center', marginVertical: HP('10') }}
                                        />
                                }

                            </View>
                            :
                            null
                    }
                </ScrollView>
                <Modal
                    animationInTiming={200}
                    animationOutTiming={100}
                    animationIn="slideInLeft"
                    animationOut="slideOutRight"
                    avoidKeyboard={true}
                    transparent={true}
                    isVisible={showModal}
                    onBackdropPress={this.toggleModal}
                    style={{ flex: 1, justifyContent: 'center' }}
                >
                    <View style={{ width: WP('80'), alignSelf: 'center', backgroundColor: colors.white, borderRadius: WP('5'), overflow: 'hidden' }}>
                        <View style={{ height: WP('15'), width: WP('80'), justifyContent: 'center', alignItems: 'center', backgroundColor: colors.c_bgColor }}>
                            <SmallText
                                text={ListOfStudentsStrings.deleteStudent}
                                style={{ fontSize: WP('5'), color: colors.lightBlack }}
                            />
                        </View>
                        <SmallText
                            text={ListOfStudentsStrings.detailText}
                            style={{ fontSize: WP('3'), color: colors.lightBlack, textAlign: 'center', alignSelf: 'center', width: WP('70'), marginTop: WP('5') }}
                        />
                        <TouchableOpacity style={{ height: WP('8'), width: WP('30'), backgroundColor: colors.btnColorOrange, alignSelf: 'center', justifyContent: 'center', alignItems: 'center', marginTop: WP('10'), borderRadius: WP('20'), marginBottom: WP('5') }}
                            onPress={this.toggleModal}
                        >
                            <SmallText
                                text={ListOfStudentsStrings.confirm}
                            />
                        </TouchableOpacity>
                    </View>
                </Modal>
            </View>
        );
    }
}


mapStateToProps = (state) => {
    return {
        // States
        loginRes: state.login,
        studentList: state.getParticipantslist,
    }
}
mapDispatchToProps = dispatch => {
    return {
        // Actions call
        getParticipantslistAction: (classID, token) => dispatch(getParticipantslist(classID, token))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withNavigation(StudentsList));
