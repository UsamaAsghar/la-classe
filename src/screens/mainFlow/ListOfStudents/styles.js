import { StyleSheet } from 'react-native'
import { family, size, WP, colors } from '../../../services';
export const styles = StyleSheet.create({
    scrollView: {
        flexGrow: 1
    },
    topBtnsCont: {
        height: WP('15'),
        width: WP('90'),
        justifyContent: 'space-between',
        alignSelf: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        marginTop: WP('10')
    },
    listOfStudentViewStyle: {
        height: WP('12'),
        width: WP('30'),
        alignItems: 'center',
        backgroundColor:'#ccc',
        borderRadius: 20,
        justifyContent: 'center'
    },
    classesViewStyle: {
        height: WP('12'),
        width: WP('20'),
        alignItems: 'center',
        backgroundColor:'#ccc',
        borderRadius: 20,
        justifyContent: 'center'
    },
    archiveCoursesViewStyle:{
        height: WP('12'), 
        width: WP('35'), 
        alignItems: 'center', 
        backgroundColor:'#ccc',
        justifyContent: 'center', 
        borderRadius: 20 
    },
    titleTextStyle: {
        alignSelf: 'center',
        marginTop: WP('8'),
        color: colors.drakBlack,
        fontWeight: 'normal',
        fontSize: WP('5')
    },
    ButtonsMainCont:{
        height: WP('10'), 
        width: WP('58'), 
        flexDirection: 'row', 
        alignSelf: 'flex-end',
        marginRight:WP('5'), 
        marginTop: WP('5') ,
        justifyContent:'space-between',
    },
    ExportBtnStyle: {
        alignSelf: 'center',
        marginBottom: WP('5'),
        backgroundColor: colors.black,
        width: WP('25'),
        marginLeft:WP('2'),
        height: WP('10'),
    },
    startCardBtnStyle: {
        alignSelf: 'center',
        marginBottom: WP('5'),
        backgroundColor: colors.btnColorOrange,
        width: WP('25'),
        marginRight:WP('2'),
        height: WP('10'),
    },
    textInputContView:{
        // height: WP('10'), 
        width: WP('85'), 
        flexDirection: 'row', 
        alignItems: 'center', 
        justifyContent: 'space-between', 
        backgroundColor: colors.white, 
        borderRadius: 20,
        marginTop:WP('5'),
        marginBottom:WP('5'),
        alignSelf: 'center', 
    },
    placeholderContStyle:{
        height: WP('10'), 
        marginLeft: WP('5'), 
        backgroundColor: colors.white, 
        width: WP('65') 
    },
    searchIconStyle:{
        height: WP('5'), 
        width: WP('5'), 
        alignSelf: 'center', 
        marginRight: WP('3') 
    },

// CARD
cardCont:{
    width: WP('90'), 
    alignSelf: 'center', 
    marginBottom: WP('5'), 
    overflow: 'hidden', 
    borderRadius: WP('2') 
},
cardChildView:{
    height: WP('20'), 
    width: WP('90'), 
    backgroundColor:colors.white, 
    alignSelf: 'center', 
    alignItems: 'center', 
    flexDirection: 'row',
    marginBottom:WP('0.5')
},
textViewStyle:{
    width: WP('47') 
},
nameTextStyle:{
    fontSize: WP('3') 
},
emailTextStyle:{
    fontSize: WP('3'), 
    color: colors.lightGrey 
},
clockIconViewStyle:{
    height: WP('8'), 
    width: WP('8'), 
    backgroundColor:'#ccc',
    justifyContent: 'center', 
    borderRadius: WP('1') 
},
clockIconStyle:{
    height: WP('5'), 
    width: WP('5'), 
    resizeMode: 'contain', 
    alignSelf: 'center', 
},
deleteIconViewStyle:{
    height: WP('8'), 
    width: WP('8'), 
    marginLeft: WP('2'), 
    backgroundColor:'#ccc',
    justifyContent: 'center', 
    borderRadius: WP('1') 
},
deleteIconStyle:{
    height: WP('5'), 
    width: WP('5'), 
    resizeMode: 'contain', 
    alignSelf: 'center',
},


});