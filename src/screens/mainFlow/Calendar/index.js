import React, { Component } from 'react';
import { connect } from 'react-redux';
import { styles } from './styles'
import { BackgroundWarapper, Button, NormalText, TinyText, MediumText, SmallText, } from '../../../components';
import { colors, WP, appImages, YourProgramStrings, } from '../../../services';
import { withNavigation } from 'react-navigation';
import { Image, View, ScrollView, TouchableOpacity } from 'react-native';
import { Calendar, CalendarList, Agenda } from 'react-native-calendars';
var moment = require('moment');

class Calender extends Component {
    constructor(props) {
        super(props);
        this.state = {
            //states
            dateSelected: {}
        }
    }
    render() {
        const { getNotifications } = this.props
        const { userProfile } = this.props.loginRes;
        var date = new Date(); // M-D-YYYY
        var d = date.getDate();
        var m = date.getMonth() + 1;
        var y = date.getFullYear();
        var dateString = y + '-' + (m <= 9 ? '0' + m : m) + '-' + (d <= 9 ? '0' + d : d);
        return (
            <BackgroundWarapper
                isProfile={true}
                userName={userProfile.fullName}
                email={userProfile.email}
                onPressProfile={() => this.props.navigation.navigate('Profile')}
                notifications={getNotifications.notificationsData ? getNotifications.notificationsData.notifications : null}
            >
                <MediumText
                    text={YourProgramStrings.title}
                    style={{ fontSize: WP('6'), alignSelf: 'center', marginTop: WP('7') }}
                />
                <View style={styles.container}>
                    <View style={{ height: WP('15'), width: WP('90'), backgroundColor: colors.drakBlack, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
                        <Button
                            isRightShowIcon={true}
                            image={appImages.BlackPlusIcon}
                            title={YourProgramStrings.creatLesson}
                            titleStyle={{ color: colors.black }}
                            titleContainerStyle={{ width: WP('30'), marginLeft: WP('-5') }}
                            style={styles.creatLessonBtnStyle}
                            onPress={() => this.props.navigation.navigate('ClassCreation')}
                        />
                    </View>
                    <View style={{ width: WP('90'), backgroundColor: colors.white, alignSelf: 'center', borderRadius: 5, overflow: 'hidden', justifyContent: 'center' }}>
                        <Calendar
                            markedDates={this.state.dateSelected}
                            // Initially visible month. Default = Date()
                            // current={date}
                            // Minimum date that can be selected, dates before minDate will be grayed out. Default = undefined
                            // minDate={dateString}
                            // Maximum date that can be selected, dates after maxDate will be grayed out. Default = undefined
                            // maxDate={'2022-01-01'}
                            // Handler which gets executed on day press. Default = undefined
                            onDayPress={(day) => {
                                this.setState({
                                    dateSelected: { [day.dateString]: { selected: true, selectedColor: colors.black } },
                                    selectedDay: day.dateString
                                }, () => {
                                    let d1 = moment(day.dateString);
                                    let date = d1.format('ll');
                                    console.log("Formated: ", date)
                                })
                            }}
                            // Handler which gets executed on day long press. Default = undefined
                            onDayLongPress={(day) => { console.log('selected day', day) }}
                            // Month format in calendar title. Formatting values: http://arshaw.com/xdate/#Formatting
                            // monthFormat={'yyyy MM'}
                            // Handler which gets executed when visible month changes in calendar. Default = undefined
                            onMonthChange={(month) => { console.log('month changed', month) }}
                            // Hide month navigation arrows. Default = false
                            hideArrows={false}
                            // Replace default arrows with custom ones (direction can be 'left' or 'right')
                            // renderArrow={(direction) => {
                            //     console.log('direction: ',direction);
                            //     return(
                            //         <View style={{ height: WP('5'), width: WP('80'), backgroundColor:'red' }}>

                            //         </View>
                            //     )
                            // }}
                            // Do not show days of other months in month page. Default = false
                            hideExtraDays={true}
                            // If hideArrows=false and hideExtraDays=false do not switch month when tapping on greyed out
                            // day from another month that is visible in calendar page. Default = false
                            disableMonthChange={false}
                            // If firstDay=1 week starts from Monday. Note that dayNames and dayNamesShort should still start from Sunday.
                            firstDay={1}
                            // Hide day names. Default = false
                            hideDayNames={false}
                            // Show week numbers to the left. Default = false
                            showWeekNumbers={true}
                            // Handler which gets executed when press arrow icon left. It receive a callback can go back month
                            onPressArrowLeft={substractMonth => substractMonth()}
                            // Handler which gets executed when press arrow icon right. It receive a callback can go next month
                            onPressArrowRight={addMonth => addMonth()}
                            // Disable left arrow. Default = false
                            disableArrowLeft={false}
                            // Disable right arrow. Default = false
                            disableArrowRight={false}
                            style={{
                                // height:WP('100')
                                marginTop: WP('3'),
                                marginBottom: WP('5')
                            }}
                            theme={{
                                backgroundColor: '#ffffff',
                                calendarBackground: '#ffffff',
                                textSectionTitleColor: colors.black,
                                selectedDayBackgroundColor: 'white',
                                selectedDayTextColor: 'white',
                                todayTextColor: 'black',
                                // todayBackgroundColor: 'silver',
                                dayTextColor: 'black',
                                textDisabledColor: '#d9e1e8',
                                dotColor: '#00adf5',
                                arrowColor: 'black',
                                monthTextColor: 'black',
                                textDayFontSize: 14,
                                textMonthFontSize: 12,
                                textDayHeaderFontSize: 12,
                            }}
                        />
                    </View>
                </View>


            </BackgroundWarapper>
        );
    }
}


mapStateToProps = (state) => {
    return {
        loginRes: state.login,
        getNotifications: state.getnotifications,
        // States
    }
}
mapDispatchToProps = dispatch => {
    return {

        // Actions call
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withNavigation(Calender));
