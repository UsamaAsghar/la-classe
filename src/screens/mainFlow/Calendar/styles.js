import { StyleSheet } from 'react-native'
import { WP, colors } from '../../../services';
export const styles = StyleSheet.create({
    scrollView: {
        flexGrow: 1
    },
    container: {
        width: WP('90'), 
        alignSelf: 'center', 
        backgroundColor: colors.red, 
        marginTop: WP('5'), 
        borderRadius: WP('2'), 
        // overflow: 'hidden',
        ...Platform.select({
            ios: {
                shadowColor: colors.lightGrey,
                shadowOffset: { width: 0, height: 2 },
                shadowOpacity: 0.5,
            },
            android: {
                elevation: 2
            },
        }),
    },
    creatLessonBtnStyle: {
        backgroundColor: colors.btnColorOrange,
        width: WP('45'),
        height: WP('10'),
        marginLeft:WP('3')
    },
});