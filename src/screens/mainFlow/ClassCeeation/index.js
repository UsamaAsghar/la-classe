import React, { Component } from 'react';
import { connect } from 'react-redux';
import { styles } from './styles'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { BackgroundWarapper, MediumText, DropDownCard, InputField, Button, SmallText } from '../../../components';
import { colors, WP, classCreationStrings, appImages } from '../../../services';
import { withNavigation } from 'react-navigation';
import { View, Image } from 'react-native';
import Toast from 'react-native-simple-toast'
import { TouchableOpacity } from 'react-native-gesture-handler';
import { createClass } from '../../../store/actions'

const cities = [
    { label: "Dubai", value: 'Dubai' },
    { label: "Abu Dhabi", value: 'Abu Dhabi' },
    { label: "Sharjah", value: 'Sharjah' },
    { label: "Ajman", value: 'Ajman' },
    { label: "Al Ain", value: 'Al Ain' },
    { label: "Fujairah", value: 'Fujairah' },
    { label: "Ras Al Khaimah", value: 'Ras Al Khaimah' }]

class ClassCreation extends Component {
    constructor(props) {
        super(props);
        this.state = {
            //states
            className: '',
            schoolName: '',
            cityName: '',
        }
    }
    componentWillReceiveProps (props) {
        const { classDataRes } = props;
        // console.log('[CreateClass.js] componentWillReceiveProps======:',classDataRes);  
        if (classDataRes.isSuccess) {
            classDataRes.isSuccess = false;
            // this.props.navigation.push('MyClasses')
            props.navigation.goBack()
        } else {
            //Failer
            if (classDataRes.isFailure) {
                Toast.show('Network error')
            }
        }     
    }
    createClass = async() => {
        const { token } = this.props.loginRes;
        const { className, schoolName, cityName } = this.state;
        if (className == '') {
            Toast.show('Please enter your class name.')
        } else {
            if (schoolName == '') {
                Toast.show('Please enter your school name.')
            } else {
                if (cityName == '') {
                    Toast.show('Please select your city name.')
                } else {
                    let params = {
                        classeName: className,
                        schoolName: schoolName,
                        city: cityName,
                        invitedUsers: [
                            "name;email@gmail.com"
                        ]
                    }
                    await this.props.createClassAction(params, token)
                    // console.log('params are===========:', params, token);
                }
            }
        }
    }

    render() {
        const { classDataRes, getNotifications } = this.props;
        const  { userProfile } = this.props.loginRes;
        return (
            <KeyboardAwareScrollView
                contentContainerStyle={styles.scrollView}
            >
                <BackgroundWarapper 
                    isProfile={true}
                    isBackBtn={true}
                    userName={userProfile.fullName} 
                    email={userProfile.email}
                    onBackBtn={()=> this.props.navigation.goBack()}
                    notifications={getNotifications.notificationsData ? getNotifications.notificationsData.notifications : null}
                    >
                    <MediumText
                        text={classCreationStrings.title}
                        style={styles.titleTextStyle}
                    />
                    <View style={styles.childViewsContStyle}>
                        <TouchableOpacity style={styles.classCreationTextContStyle}>
                            <View style={styles.classCreationImgCont}>
                                <Image
                                    source={appImages.StudentsIcon}
                                    style={styles.classCreationImgStyle}
                                />
                            </View>
                            <SmallText
                                text={classCreationStrings.classCreation}
                                style={styles.classCreationTextStyle}
                            />
                        </TouchableOpacity>
                        <Image
                            source={appImages.NextBtnIcon}
                            style={styles.nextBtnIconStyle}
                        />
                        <TouchableOpacity style={styles.addingStudentContStyle}
                            onPress={() => this.props.navigation.navigate('AddStudents')}
                        >
                            <View style={styles.addingStudentsIconCont}>
                                <Image
                                    source={appImages.profileblackIcon}
                                    style={styles.addingStudentIconStyle}
                                />
                            </View>
                            <SmallText
                                text={classCreationStrings.addingStudent}
                                style={styles.addingStudentTextStyle}
                            />
                        </TouchableOpacity  >
                    </View>
                    <InputField
                        isShowIcon={false}
                        placeholder={classCreationStrings.classNameString}
                        onChangeText={(value)=> this.setState({ className: value })}
                        styles={styles.classnameInputStyle}
                    />
                    <InputField
                        isShowIcon={false}
                        placeholder={classCreationStrings.establishmentString}
                        onChangeText={(value)=> this.setState({ schoolName: value })}
                        styles={styles.establishmentNameInputFieldStyle}
                    />
                    <DropDownCard
                        child={false}
                        headerEnable={false}
                        dropDownOptions={cities}
                        label={'City'}
                        dropdownContainer={{}}
                        style={{ marginTop: WP('5') }}
                        onSelectItem={(index, value) => { this.setState({ cityName: value }) }}
                    />
                    <Button
                        showLoader={classDataRes.loading}
                        title={classCreationStrings.confirmString}
                        titleStyle={{ color: colors.black }}
                        style={styles.confirmBtnStyle}
                        onPress={() => this.createClass()}
                    />
                </BackgroundWarapper>
            </KeyboardAwareScrollView>
        );
    }
}


mapStateToProps = (state) => {
    return {
        // 
        loginRes: state.login,
        classDataRes: state.createClass,
        getNotifications: state.getnotifications,
    }
}
mapDispatchToProps = dispatch => {
    return {
        // Actions call
        createClassAction: (params, token) => dispatch(createClass(params, token))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withNavigation(ClassCreation));
