import { StyleSheet } from 'react-native'
import { family, size, WP, colors } from '../../../services';
export const styles = StyleSheet.create({
    scrollView:{
        flexGrow:1
    },
    titleTextStyle:{
        alignSelf: 'center', 
        marginTop: WP('10'), 
        color: colors.drakBlack, 
        fontWeight: 'normal' 
    },
    childViewsContStyle:{
        height: WP('15'), 
        width: WP('90'), 
        flexDirection: 'row', 
        alignItems: 'center', 
        alignSelf: 'center', 
        marginTop: WP('5'), 
        justifyContent: 'space-between' 
    },
    classCreationTextContStyle:{
        height: WP('13'), 
        width: WP('40'), 
        flexDirection: 'row', 
        alignItems: 'center',
    },
    classCreationImgCont:{
        height: WP('10'), 
        width: WP('10'), 
        borderRadius: 30, 
        backgroundColor: colors.btnColorOrange, 
        marginHorizontal: WP('2'), 
        justifyContent: 'center' 
    },
    classCreationImgStyle:{
        height: WP('6'), 
        width: WP('6'), 
        resizeMode: 'contain', 
        alignSelf: 'center', 
    },
    classCreationTextStyle:{
        fontSize: WP('3.5') 
    },
    nextBtnIconStyle:{
        height: WP('2'), 
        width: WP('3') 
    },
    addingStudentContStyle:{
        height: WP('13'), 
        width: WP('42'), 
        flexDirection: 'row', 
        alignItems: 'center',
        },
    addingStudentsIconCont:{
        height: WP('10'), 
        width: WP('10'), 
        borderRadius: 30, 
        backgroundColor: colors.white, 
        marginHorizontal: WP('2'), 
        justifyContent: 'center' 
    },
    addingStudentIconStyle:{
        height: WP('10'), 
        width: WP('10'), 
        resizeMode: 'contain', 
        alignSelf: 'center', 
    },
    addingStudentTextStyle:{
        fontSize: WP('3.5') 
    },
    subordinateTextStyle:{
        alignSelf: 'center', 
        marginTop: WP('2'), 
        color: colors.mediumGrey 
    },
    confirmBtnStyle:{
        alignSelf: 'center', 
        marginTop: WP('10'), 
        marginBottom: WP('10'), 
        backgroundColor: colors.btnColorOrange 
    },
    classnameInputStyle:{
        marginTop: WP('5') 
    },
    establishmentNameInputFieldStyle:{
        marginTop: WP('5') 
    },
    
});