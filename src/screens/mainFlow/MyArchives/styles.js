import { StyleSheet } from 'react-native'
import { family, size, WP, colors } from '../../../services';
export const styles = StyleSheet.create({
    scrollView: {
        flexGrow: 1
    },
    titleTextStyle: {
        fontWeight: 'normal',
        marginTop: WP('10'),
        fontSize: WP('6'),
        alignSelf: 'center'
    },
    subTitleStyle: {
        fontWeight: 'normal',
        marginTop: WP('6'),
        marginHorizontal: WP('5')
    },
    //Card
    cardContView:{
        width: WP('90'), 
        backgroundColor: colors.white, 
        alignSelf:'center', 
        borderRadius:WP('2'),
        marginTop:WP('4'), 
        alignItems: 'center',
        overflow:'hidden' 
    },
    courseDetailView:{
        height: WP('16'), 
        width: WP('90'), 
        borderBottomWidth: WP('0.1'), 
        borderBottomColor: colors.lightGrey, 
        alignItems: 'center', 
        flexDirection: 'row' 
    },
    capIconView:{
        height: WP('10'), 
        width: WP('10'), 
        borderRadius: 50, 
        backgroundColor: colors.btnColorOrange, 
        marginHorizontal: WP('2'), 
        alignItems: 'center', 
        justifyContent: 'center' 
    },
    capIconStyle:{
        height: WP('5'), 
        width: WP('6'), 
        resizeMode: 'contain'
    },
    courseOneTextStyle:{
        color: colors.black, 
        width: WP('70')
    },
    createdByTextStyle:{
        color: colors.lightGrey
    },
    creatorNameTextStyle:{
        color: colors.mediumGrey, 
        fontWeight: 'normal'
    },
    menuIconStyle:{
        height: WP('5'), 
        color:colors.mediumGrey, 
        width: WP('6'), 
        resizeMode: 'contain' 
    },
    startCourseViewStyle:{
        height: WP('14'), 
        width: WP('90'), 
        flexDirection: 'row', 
        alignItems: 'center', 
        borderBottomWidth: WP('0.1'), 
        borderBottomColor: colors.lightGrey 
    },
    clockIconView:{
        height: WP(6), 
        width: WP(7), 
        resizeMode: 'contain', 
        marginLeft: WP('3') 
    },
    startCourseTextStyle:{
        color: colors.lightGrey, 
        marginLeft: WP('3') 
    },
    documentDetailViewStyle:{
        height: WP('14'), 
        width: WP('90'), 
        flexDirection: 'row', 
        alignItems: 'center', 
        borderBottomWidth: WP('0.1'), 
        borderBottomColor: colors.lightGrey 
    },
    documentIconStyle:{
        height: WP('6'), 
        width: WP('7'), 
        resizeMode: 'contain', 
        marginLeft: WP('3') 
    },
    documentDetailTextStyle:{
        color: colors.lightGrey, 
        width: WP('70'), 
        marginLeft: WP('3')
    },
    achivedTextBtnStyle:{
        height: WP('10'), 
        width: WP('80'), 
        marginVertical: WP('5'), 
        alignSelf: 'center', 
        alignItems: 'center', 
        justifyContent: 'center', 
        backgroundColor:colors.black, 
        borderRadius: 30, 
    },
    achivedTextStyle:{
        color:colors.white
    }

});