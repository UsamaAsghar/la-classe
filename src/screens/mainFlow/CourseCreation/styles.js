import { StyleSheet } from 'react-native'
import { family, size, WP, colors } from '../../../services';
export const styles = StyleSheet.create({
    scrollView: {
        flexGrow: 1
    },
    titleTextStyle: {
        alignSelf: 'center',
        marginTop: WP('10'),
        color: colors.drakBlack,
        fontWeight: 'normal'
    },
    courseNameInputFieldStyle: {
        marginTop: WP('10')
    },
    discriptionInputFieldStyle: {
        marginTop: WP('5') 
    },
    classesInputFiedStyle: {
        marginTop: WP('5')
    },
    startCourseInputField: {
        marginTop: WP('5')
    },
    coursedetailMainCont: {
        flexDirection: 'row',
        height: WP('22'),
        width: WP('90'),
        alignSelf: 'center',
        marginTop: WP('5'),
        alignItems: 'center',
        justifyContent: 'center',
    },
    courseEndDateCont: {
        height: WP('22'),
        width: WP('43'),
        alignItems: 'flex-start',
        marginLeft: WP('2')
    },
    calanderCont: {
        height: WP('12'),
        width: WP('42'),
        borderRadius: 10,
        alignItems: 'center',
        marginTop: WP('1'),
        flexDirection: 'row',
        backgroundColor: colors.white,
    },
    timeTextStyle: {
        marginVertical: WP('1')
    },
    calanderIconStyle: {
        height: WP('6'),
        width: WP('8'),
        resizeMode: 'contain',
        marginHorizontal: WP('1')
    },
    BtnMainContainer:{
        height:WP('12'), 
        width:WP('90'), 
        alignItems:'center', 
        alignSelf:'center', 
        justifyContent:'space-between', 
        marginTop:WP('5') ,
        flexDirection:'row'
    },
    uploadBtnStyle: {
        alignSelf: 'center',
        height:WP('10'),
        width:WP('44'),
        backgroundColor: colors.black
    },
    confirmBtnStyle: {
        alignSelf: 'center',
        height:WP('10'),
        width:WP('44'),
        backgroundColor: colors.btnColorOrange
    },
});