import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, } from 'react-native'
import { styles } from './styles'
import DatePicker from 'react-native-datepicker'
import Toast from 'react-native-simple-toast'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { BackgroundWarapper, MediumText, DropDownCard, NormalText, SmallText, InputField, Button, CustomInputField } from '../../../components';
import { colors, WP, appImages, classCreationStrings, courseCreationStrings } from '../../../services';
import { withNavigation } from 'react-navigation';
import { createCourses, getClasses } from '../../../store/actions';



const cities = [
    { label: "No Classes", value: '' },
    ]

const instantPicker = [
    { label: "Yes", value: 'Yes' },
    { label: "No", value: 'No' }]

class CourseCreation extends Component {
    constructor(props) {
        super(props);
        this.state = {
            //states
            classID: '',   //'Select city'
            instant: '',   //'Select an option'
            courseName: '',
            description: '',
            startDate: '',
            endDate: '',
            classes: []
        }
    }
    componentDidMount = async () => {
        const { token } = this.props.loginRes;
        await this.props.getClassesAction(token)
    }
    
    componentWillReceiveProps(props) {
        const { createCourse, classesData } = props;
        // console.log('[CreateCourse.js] componentWillReceiveProps======:', createCourse);
        if (createCourse.isSuccess) {
            createCourse.isSuccess = false;
            // this.props.navigation.push('MyCourses')
            this.props.navigation.goBack()
        } else {
            //Failer
            if (createCourse.isFailure) {
                Toast.show('Network error')
            }
        }
        // console.log('[classesData] componentWillReceiveProps======:', classesData);
        if (classesData.isSuccess) {
            props.classesData.isSuccess = false;
            classesData.classesData && classesData.classesData.Classes.forEach(item => {
                this.state.classes.push({ label: item.classeName, value: item.id })
            });
        } else {
            
        }
    }
    createCourse = async () => {
        const { token } = this.props.loginRes;
        const { courseName, description, startDate, endDate, classID, instant } = this.state;
        if (courseName == '') {
            Toast.show('Please enter your course name.')
        } else {
            if (description == '') {
                Toast.show('Please select your description.')
            } else {
                if (classID == '') {
                    Toast.show('Please select your city.')
                } else {
                    if (instant == '') {
                        Toast.show('Please select if you want to start the course now.')
                    } else {
                        if (startDate == '') {
                            Toast.show('Please select your startDate.')
                        } else {
                            if (endDate == '') {
                                Toast.show('Please select your endDate.')
                            } else {
                                let params = {
                                    roomName: courseName,
                                    startDateTime: startDate,
                                    description: description,
                                    classeId: '5ec434034c14c67dce0c3c43',
                                    isInstant: instant == 'Yes'? true:false,
                                    endDateTime: endDate
                                }
                                await this.props.creatCoursesAction(params, token)
                                console.log('params are===========:', params, token);
                            }
                        }
                    }
                }
            }
        }
    }
    // }

    render() {
        const { classes } = this.state;
        const { createCourse, getNotifications } = this.props;
        const { classesData } = this.props.classesData;
        const  { userProfile } = this.props.loginRes;
        return (
            <KeyboardAwareScrollView
                contentContainerStyle={styles.scrollView}
            >
                <BackgroundWarapper 
                     isProfile={true}
                     isBackBtn={true}
                     isLoading={this.props.classesData.loading}
                     userName={userProfile.fullName} 
                     email={userProfile.email}
                     onBackBtn={()=> this.props.navigation.goBack()}
                     notifications={getNotifications.notificationsData ? getNotifications.notificationsData.notifications : null}
                    >
                    <MediumText
                        text={courseCreationStrings.title}
                        style={styles.titleTextStyle}
                    />
                    <InputField
                        isShowIcon={false}
                        placeholder={courseCreationStrings.courseName}
                        styles={styles.courseNameInputFieldStyle}
                        onChangeText={(value) => this.setState({ courseName: value })}
                    />
                    <InputField
                        isShowIcon={false}
                        placeholder={courseCreationStrings.description}
                        styles={styles.discriptionInputFieldStyle}
                        onChangeText={(value) => this.setState({ description: value })}
                    />
                    <DropDownCard
                        placeholderText={'Select City'}
                        dropDownOptions={classes.length > 0? classes : classes}
                        label={'City'}
                        dropdownContainer={{}}
                        style={{ marginTop: WP('5') }}
                        onSelectItem={(index, value) => { this.setState({ classID: value }) }}
                    />
                    <DropDownCard
                        dropDownOptions={instantPicker}
                        label={'City'}
                        dropdownContainer={{}}
                        style={{ marginTop: WP('5') }}
                        onSelectItem={(index, value) => { this.setState({ instant: value }) }}
                    />
                    <View style={styles.coursedetailMainCont}>

                        <View style={styles.courseEndDateCont}>
                            <NormalText
                                text={courseCreationStrings.timeStartText}
                                style={styles.timeTextStyle}
                            />
                            <CustomInputField
                                value={''}
                                date={this.state.startDate}
                                isCalender={true}
                                placeholderText={'1990/01/20'}
                                placeholderTextColor={colors.lightGrey}
                                isRightIcon={true}
                                iconName='calendar'
                                iconColor={colors.black}
                                setDate={(date) => this.setState({ startDate: date })}
                                style={{ paddingHorizontal: WP('1') }}
                                containerStyle={styles.calanderCont}
                            />
                        </View>

                        <View style={styles.courseEndDateCont}>
                            <NormalText
                                text={courseCreationStrings.timeEndText}
                                style={styles.timeTextStyle}
                            />
                            <CustomInputField
                                value={''}
                                date={this.state.endDate}
                                isCalender={true}
                                placeholderText={'1990/01/20'}
                                placeholderTextColor={colors.lightGrey}
                                isRightIcon={true}
                                iconName='calendar'
                                iconColor={colors.black}
                                setDate={(date) => this.setState({ endDate: date })}
                                style={{ paddingHorizontal: WP('1') }}
                                containerStyle={styles.calanderCont}
                            />
                        </View>
                    </View>
                    <View style={styles.BtnMainContainer}>
                        <Button
                            isRightShowIcon={true}
                            image={appImages.UploadIcon}
                            title={classCreationStrings.uploadString}
                            titleContainerStyle={{ width: WP('20') }}
                            titleStyle={{ color: colors.white }}
                            style={styles.uploadBtnStyle}
                            onPress={() => this.props.navigation.navigate('MyCourses')}
                        />
                        <Button
                            showLoader={createCourse.loading}
                            isRightShowIcon={true}
                            image={appImages.TickBtnIcon}
                            title={classCreationStrings.confirmString}
                            titleContainerStyle={{ width: WP('18') }}
                            titleStyle={{ color: colors.black }}
                            style={styles.confirmBtnStyle}
                            onPress={() => this.createCourse()}
                        />
                    </View>
                </BackgroundWarapper>
            </KeyboardAwareScrollView>
        );
    }
}


mapStateToProps = (state) => {
    return {
        // States
        loginRes: state.login,
        createCourse: state.creatcourses,
        getNotifications: state.getnotifications,
        classesData: state.getClasses
    }
}
mapDispatchToProps = dispatch => {
    return {
        // Actions call
        getClassesAction: (token) => dispatch(getClasses(token)),
        creatCoursesAction: (params,token) => dispatch(createCourses(params,token)),

    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withNavigation(CourseCreation));
