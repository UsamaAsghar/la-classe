import { StyleSheet } from 'react-native'
import { family, size, WP, colors } from '../../../services';
export const styles = StyleSheet.create({
    scrollView: {
        flexGrow: 1
    },
    titleTextStyle: {
        alignSelf: 'center',
        marginTop: WP('8'),
        color: colors.drakBlack,
        fontWeight: 'normal',
        fontSize: WP('5')
    },
    coursesDetailTextStyle: {
        alignSelf: 'flex-start',
        marginTop: WP('5'),
        color: colors.Black,
        fontWeight: 'normal',
        marginLeft: WP('6')
    },
    archivedTextView:{
        height: WP('7'), 
        width: WP(35), 
        backgroundColor: colors.lightBlack, 
        marginTop: WP('4'), 
        justifyContent:'center', 
        alignItems:'center', 
        borderRadius: 50 
    },
    archivedTextStyle:{
        color:colors.white
    },
    container: {
        // height: WP('100'), 
        width: WP('90'),
        alignSelf: 'center',
        overflow: 'hidden',
        marginHorizontal: WP('3'),
        backgroundColor: colors.white,
        marginTop: WP('5'),
        borderRadius: 5,
        ...Platform.select({
            ios: {
                shadowColor: '#000',
                shadowOffset: { width: 0, height: 2 },
                shadowOpacity: 0.2,
            },
            android: {
                elevation: 1
            },
        }),
    },
    classplaceholderImgStyle: {
        height: WP('50'),
        width: WP('90'),
        resizeMode: 'contain',
    },
    classPlaceHolderCont: {
        height: WP('50'),
        width: WP('90'),
        alignSelf: 'center',
        backgroundColor: colors.white,
        marginTop: WP('8')
    },
    noCoursesTextStyle: {
        alignSelf: 'center',
        marginTop: WP('15'),
        fontSize: WP('6')
    },
    creatCourseBtnStyle: {
        alignSelf: 'center',
        marginBottom: WP('10'),
        marginTop: WP('5'),
        backgroundColor: colors.btnColorOrange,
        width: WP('80')
    },
    /// card Style
    cardContainer: {
        width: WP('90'),
        backgroundColor: colors.white,
        alignSelf: 'center',
        borderRadius: WP('2'),
        marginVertical: WP('2'),
        ...Platform.select({
            ios: {
                shadowColor: '#000',
                shadowOffset: { width: 0, height: 2 },
                shadowOpacity: 0.2,
            },
            android: {
                elevation: 1
            },
        }),
    },
    cardHeader: {
        height: WP('20'),
        width: WP('90'),
        alignItems: 'center',
        flexDirection: 'row',
        borderBottomWidth: WP('0.1'),
        borderBottomColor: colors.lightGrey
    },
    cardHeaderIconView: {
        height: WP('11'),
        width: WP('11'),
        borderRadius: 50,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: colors.btnColorOrange,
        marginHorizontal: WP('3')
    },
    cardHeaderIconStyle: {
        height: WP('5'),
        width: WP('8'),
        resizeMode: 'contain',
        marginTop: WP('1')
    },
    headerTextStyle: {
        color: colors.lightBlack,
    },
    headerSubtitalStyle: {
        color: colors.lightGrey,
    },
    headerTextCreatorStyle: {
        color: colors.mediumGrey,
    },
    menuIconStyle: {
        height: WP('6'),
        width: WP('8'),
        resizeMode: 'contain'
    },
    cardDetailsCont: {
        height: WP('12'),
        width: WP('90'),
        flexDirection: 'row',
        alignItems: 'center',
        flexDirection: 'row',
        borderBottomWidth: WP('0.1'),
        borderBottomColor: colors.lightGrey
    },
    clockIconView: {
        height: WP('10'),
        width: WP('10'),
        borderRadius: 50,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: colors.white,
        marginHorizontal: WP('3')
    },
    clockIconStyle: {
        height: WP('6'),
        width: WP('8'),
        resizeMode: 'contain',
    },
    startCourseTextStyle: {
        color: colors.mediumGrey
    },
    documentIconView: {
        height: WP('10'),
        width: WP('10'),
        borderRadius: 50,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: colors.white,
        marginHorizontal: WP('3')
    },
    documentDetailTextStyle: {
        color: colors.mediumGrey,
        fontSize: WP('3')
    },

    copyIconStyle:{
        height:WP('5'),
        width:WP('3'),
        resizeMode:'contain',
        marginHorizontal:WP('3')
    },
    copyBtnTextStyle:{
        color:colors.lightBlack,
        fontSize:WP('2.5')
    },

    nextArrowIconView: {
        height: WP('10'),
        width: WP('10'),
        borderRadius: 50,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: colors.white,
        marginHorizontal: WP('3')
    },
    nextArrowIconStyle: {
        height: WP('5'),
        width: WP('8'),
        resizeMode: 'contain',
    },
    onlineAddressCopyBtnView: {
        alignSelf: 'center',
        backgroundColor: colors.c_bgColor,
        width: WP('23'),
        height: WP('8'),
        borderRadius: WP('1'),
    },
    testClassViewsMainCont: {
        height: WP('12'),
        width: WP('90'),
        marginTop: WP('3'),
        alignItems: 'center',
        flexDirection: 'row'
    },
    testClassViewCont: {
        height: WP('8'),
        width: WP('24'),
        flexDirection: "row",
        justifyContent: 'center',
        marginLeft: WP('3'),
        alignItems: 'center',
        borderRadius: WP('1'),
        backgroundColor: colors.c_bgColor,
    },
    studentsIconView:{
        height: WP('6'), 
        width: WP('6'), 
        borderRadius: 50, 
        alignItems: 'center', 
        justifyContent: 'center',
    },
    studentIconStyle: {
        height: WP('3'),
        width: WP('5'),
        resizeMode: 'contain',
        marginHorizontal: WP('1'),
        color: colors.lightBlack
    },
    startCardBtnStyle: {
        alignSelf: 'center',
        marginBottom: WP('5'),
        marginTop: WP('5'),
        marginHorizontal:WP('2'),
        backgroundColor: colors.btnColorOrange,
        width: WP('38'),
        height: WP('10'),
    },
    downloadCardBtnStyle: {
        alignSelf: 'center',
        marginBottom: WP('5'),
        marginTop: WP('5'),
        backgroundColor: colors.black,
        width: WP('38'),
        height: WP('10')
    },
});