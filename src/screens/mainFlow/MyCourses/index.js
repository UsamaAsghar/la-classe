import React, { Component } from 'react';
import { Image, View, ScrollView, TouchableOpacity, Linking } from 'react-native';
import { connect } from 'react-redux';
import Toast from 'react-native-simple-toast'
import { BackgroundWarapper, NormalText, TinyText, MediumText, SmallText, Button } from '../../../components';
import { colors, WP, appImages, myCoursesStrings, myCoursesCardStrings } from '../../../services';
import { withNavigation } from 'react-navigation';
import { styles } from './styles'
import { Colors } from 'react-native/Libraries/NewAppScreen';
import { getCourses, startCourse } from '../../../store/actions';

class MyCourses extends Component {
    constructor(props) {
        super(props);
        this.state = {
            arr: [{}, {}],
            isClasses: false,
            city: '',   //'Select city'
            instant: '',   //'Select an option'
            courseName: '',
            description: '',
            startDate: '',
            endDate: '',
            //states
        }
    }
    componentDidMount = async () => {
        const { navigation } = this.props;
        const { token } = this.props.loginRes;
        this.focusListener = navigation.addListener("didFocus", async() => {
            // The screen is focused
            // Call any action
            await this.props.getCoursesAction(token)
        });
    }
    componentWillReceiveProps(props) {
        console.log('[MyCourses.js] ======:', props);
        const { startCourse } = props;
        if (startCourse.isSuccess) {
            props.startCourse.isSuccess = false;
            if (startCourse.startCourse.isModerator) {
                Toast.show('You have no access to start.')
            } else {
                Linking.openURL(startCourse.startCourse.roomURL)
            }
        } else {
            if (startCourse.isFailure) {
                Toast.show('Please try again.')
            }
        }
    }

    startRoom = async (roomCode) => {
        const { startCourseAction, loginRes } = this.props;
        const { token } = this.props.loginRes;
        let params = {
            roomCode: roomCode,
            userEmail: loginRes.userProfile.email
        }
        await startCourseAction(params, token);
    }

    classCard = (item, key) => {
        const { startCourse } = this.props;
        return (
            <View style={styles.cardContainer}>
                <View style={styles.cardHeader}>
                    <View style={styles.cardHeaderIconView}>
                        <Image
                            source={appImages.CapIcon}
                            style={styles.cardHeaderIconStyle}
                        />
                    </View>
                    <View>
                        <NormalText
                            text={item.roomName}
                            style={styles.headerTextStyle}
                        />
                        <View style={{ flexDirection: 'row' }}>
                            <SmallText
                                text={myCoursesCardStrings.createdby}
                                style={styles.headerSubtitalStyle}
                            />
                            <SmallText
                                text={item.creator.fullName}
                                style={styles.headerTextCreatorStyle}
                            />
                        </View>
                    </View>

                    <View style={{ width: WP('28') }}></View>

                    <TouchableOpacity>
                        <Image
                            source={appImages.MenuIcon}
                            style={styles.menuIconStyle}
                        />
                    </TouchableOpacity>
                </View>
                <View style={styles.cardDetailsCont}>
                    <View style={styles.clockIconView}>
                        <Image
                            source={appImages.ClockIcon}
                            style={styles.clockIconStyle}
                        />
                    </View>
                    {
                        item.isInstant ?
                            <NormalText

                                text={myCoursesCardStrings.startCourse}
                                style={styles.startCourseTextStyle}
                            />
                            :
                            <NormalText

                                text={myCoursesCardStrings.startCourseLater}
                                style={styles.startCourseTextStyle}
                            />
                    }
                </View>
                <View style={styles.cardDetailsCont}>
                    <View style={styles.documentIconView}>
                        <Image
                            source={appImages.DocumentIcon}
                            style={styles.clockIconStyle}
                        />
                    </View>
                    <View style={{ width: WP('70') }}>
                        <TinyText
                            text={item.description}
                            style={styles.startCourseTextStyle}
                        />
                    </View>
                </View>
                <View style={styles.cardDetailsCont}>
                    <View style={styles.nextArrowIconView}>
                        <Image
                            source={appImages.NextArrowIcon}
                            style={styles.nextArrowIconStyle}
                        />
                    </View>
                    <View style={{ width: WP('50') }}>
                        <TinyText
                            text={myCoursesCardStrings.onlineAddress}
                            style={styles.startCourseTextStyle}
                        />
                    </View>
                    <TouchableOpacity style={{ height: WP('8'), width: WP('20'), backgroundColor: colors.c_bgColor, alignItems: 'center', flexDirection: 'row' }}>
                        <Image
                            source={appImages.CopyIcon}
                            style={styles.copyIconStyle}
                        />
                        <SmallText
                            text={myCoursesCardStrings.copy}
                            style={styles.copyBtnTextStyle}
                        />
                    </TouchableOpacity>
                </View>
                <View style={styles.testClassViewsMainCont}>
                    <View style={styles.testClassViewCont}>
                        <View style={styles.studentsIconView}>
                            <Image
                                source={appImages.StudentsIcon}
                                style={styles.studentIconStyle}
                            />
                        </View>
                        <SmallText
                            text={myCoursesCardStrings.testclass}
                            style={{ color: colors.black }}
                        />
                    </View>
                    <View style={styles.testClassViewCont}>
                        <View style={styles.studentsIconView}>
                            <Image
                                source={appImages.StudentsIcon}
                                style={styles.studentIconStyle}
                            />
                        </View>
                        <SmallText
                            text={myCoursesCardStrings.testclass}
                            style={{ color: colors.black }}
                        />
                    </View>
                    <View style={styles.testClassViewCont}>
                        <View style={styles.studentsIconView}>
                            <Image
                                source={appImages.StudentsIcon}
                                style={styles.studentIconStyle}
                            />
                        </View>
                        <SmallText
                            text={myCoursesCardStrings.testclass}
                            style={{ color: colors.black }}
                        />
                    </View>
                </View>
                <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                    <Button
                        showLoader={startCourse.loading}
                        isRightShowIcon={true}
                        image={appImages.PlayvedioIcon}
                        title={myCoursesCardStrings.start}
                        titleStyle={{ color: colors.black, fontSize: WP('3.3') }}
                        titleContainerStyle={{ width: WP('20') }}
                        style={styles.startCardBtnStyle}
                        onPress={() => this.startRoom(item.urlCode)}
                    />
                    {/* <Button
                        isRightShowIcon={true}
                        image={appImages.DownloadIcon}
                        title={myCoursesCardStrings.download}
                        titleStyle={{ color: colors.white, fontSize: WP('3.3') }}
                        titleContainerStyle={{ width: WP('20') }}
                        style={styles.downloadCardBtnStyle}
                    // onPress={() => this.props.navigation.navigate('ClassCreation')}
                    /> */}
                </View>
            </View>

        )
    }

    render() {
        const { coursesData } = this.props;
        const { userProfile } = this.props.loginRes;
        const { isClasses, } = this.state;
        const { getNotifications } = this.props

        return (
            <BackgroundWarapper
                isLoading={coursesData.loading}
                isProfile={true}
                isBackBtn={true}
                userName={userProfile.fullName}
                email={userProfile.email}
                onBackBtn={()=> this.props.navigation.goBack()}
                onPressProfile={() => this.props.navigation.navigate('Profile')}
                notifications={getNotifications.notificationsData ? getNotifications.notificationsData.notifications : null}
            >
                <MediumText
                    text={myCoursesStrings.title}
                    style={styles.titleTextStyle}
                />
                <View style={{ flexDirection: 'row', marginBottom: WP('5') }}>
                    <NormalText
                        text={coursesData.isSuccess ? `${coursesData.coursesData.Rooms.length} Courses` : null}
                        style={styles.coursesDetailTextStyle}
                    />
                    <View style={{ width: WP('35') }}></View>
                    <TouchableOpacity style={styles.archivedTextView}
                        onPress={() => this.props.navigation.navigate('MyArchives')}
                    >
                        <NormalText
                            text={myCoursesStrings.archivedText}
                            style={{ color: Colors.white }}
                        />
                    </TouchableOpacity>
                </View>
                <ScrollView>
                    {
                        coursesData.isSuccess && coursesData.coursesData.Rooms.length > 0 ?
                            coursesData.coursesData && coursesData.coursesData.Rooms.map((item, key) => {
                                return (this.classCard(item, key))
                            })
                            :
                            <View style={styles.container}>
                                <View style={styles.classPlaceHolderCont}>
                                    <Image
                                        source={appImages.ClassPlaceHolder}
                                        style={styles.classplaceholderImgStyle}
                                    />
                                </View>
                                <MediumText
                                    text={myCoursesStrings.noCourses}
                                    style={styles.noCoursesTextStyle}
                                />
                                <Button
                                    onPress={() => { this.setState({ isClasses: true }), this.props.navigation.navigate('CourseCreation') }}
                                    title={myCoursesStrings.creatCourse}
                                    titleStyle={{ color: colors.black }}
                                    style={styles.creatCourseBtnStyle}
                                />
                            </View>
                    }
                </ScrollView>
            </BackgroundWarapper>
        );
    }
}


mapStateToProps = (state) => {
    return {
        // States
        loginRes: state.login,
        coursesData: state.getCourses,
        startCourse: state.startCourse,
        getNotifications: state.getnotifications,
    }
}
mapDispatchToProps = dispatch => {
    return {
        // Actions call
        getCoursesAction: (token) => dispatch(getCourses(token)),
        startCourseAction: (token) => dispatch(startCourse(token)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withNavigation(MyCourses));

// ///        const { getNotifications } = this.props

// notifications={getNotifications.notificationsData ? getNotifications.notificationsData.notifications : null}

// getNotifications: state.getnotifications,
