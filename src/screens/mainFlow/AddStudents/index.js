import React, { Component } from 'react';
import { View, Image, ImageBackground, TouchableOpacity, ScrollView } from 'react-native'
import { connect } from 'react-redux';
import { BackgroundWarapper, MediumText, TinyText, Button, SmallText, InputField } from '../../../components';
import { colors, WP, MyArchivesStrings, AddStudentsStrings, appImages, HP } from '../../../services';
import { withNavigation } from 'react-navigation';
import { Colors } from 'react-native/Libraries/NewAppScreen';
import { styles } from './styles';
import { getParticipantslist } from '../../../store/actions';

//Cards
// import GetStartCard from './GetStartCard';
// import StatisticsCard from './StatisticsCard';
// import UpcomingEventsCard from './UpcomingEventsCard';

class AddStudents extends Component {
    constructor(props) {
        super(props);
        this.state = {
            arr: [{}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}]
        }
    }

    componentDidMount = async () => {
        const { token } = this.props.loginRes;
        console.log('token-------:', token);
        await this.props.getParticipantslistAction(token)
    }

    classCard = (item, key) => {
        return (

            <View style={styles.cardViewStyle}>
                <TinyText
                    text={AddStudentsStrings.studentEmailAddress}
                    style={styles.cardViewTextStyle}
                />
                <TouchableOpacity style={styles.cardViewIconContStyle}>
                    <Image
                        source={appImages.closeIcon}
                        style={styles.cardViewIconStyle}
                    />
                </TouchableOpacity>
            </View>
        )
    }


    render() {
        const { userProfile } = this.props.loginRes;
        const { getNotifications } = this.props
        return (
            <BackgroundWarapper 
                isProfile={true}
                isBackBtn={true}
                userName={userProfile.fullName}
                email={userProfile.email}
                onBackBtn={()=> this.props.navigation.goBack()}
                onPressProfile={() => this.props.navigation.navigate('Profile')}
                notifications={getNotifications.notificationsData ? getNotifications.notificationsData.notifications : null}
                >
                <ScrollView>
                    <MediumText
                        text={AddStudentsStrings.title}
                        style={styles.titleTextStyle}
                    />
                    <SmallText

                        text={AddStudentsStrings.subtitle}
                        style={styles.detailTextStyle}
                    />
                    <InputField
                        isShowIcon={false}
                        placeholder={AddStudentsStrings.namePlaceHolder}
                        styles={styles.namePlaceHolderStyle}
                    />
                    <InputField
                        isShowIcon={false}
                        placeholder={AddStudentsStrings.emailPlaceHolder}
                        styles={styles.emailPlaceHolderStyle}
                    />
                    <SmallText

                        text={AddStudentsStrings.detailText}
                        style={styles.detailOfInvitation}
                    />
                    <Button
                        isRightShowIcon={true}
                        image={appImages.AddIcon}
                        title={AddStudentsStrings.add}
                        titleStyle={{ color: colors.white,marginLeft:WP('-2')}}
                        titleContainerStyle={{ width: WP('8') }}
                        style={styles.startCardBtnStyle}
                    />
                    <View style={styles.cardContainer}>
                        {
                            this.state.arr.map((item, key) => {
                                return (this.classCard(item, key))
                            })
                        }
                    </View>
                    <TouchableOpacity style={styles.confirmBtnStyle}
                        onPress={() => this.props.navigation.goBack()}
                    >
                        <SmallText
                            text={AddStudentsStrings.confirm}
                        />
                    </TouchableOpacity>
                    <View style={{ marginBottom: WP('5') }}></View>


                </ScrollView>
            </BackgroundWarapper>
        );
    }
}


mapStateToProps = (state) => {
    return {
        loginRes: state.login,
        createCourse: state.creatcourses,
        getNotifications: state.getnotifications,
        // States
    }
}
mapDispatchToProps = dispatch => {
    return {
        // Actions call
        getParticipantslistAction: (token) => dispatch(getParticipantslist(token)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withNavigation(AddStudents));
// ///        const { getNotifications } = this.props

// notifications={getNotifications.notificationsData ? getNotifications.notificationsData.notifications : null}

// getNotifications: state.getnotifications,