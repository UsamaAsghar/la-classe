import { StyleSheet } from 'react-native'
import {  WP, colors } from '../../../services';
export const styles = StyleSheet.create({
    scrollView: {
        flexGrow: 1
    },
    titleTextStyle: {
        fontWeight: 'normal', 
        marginTop: WP('10'), 
        fontSize: WP('5'), 
        alignSelf: 'center'
    },
    detailTextStyle:{
        fontWeight: 'normal', 
        color: colors.mediumGrey, 
        marginTop: WP('3'), 
        alignSelf: 'center', 
        fontSize: WP('3.2'), 
        marginHorizontal: WP('5') 
    },
    namePlaceHolderStyle:{
        marginTop: WP('5')
    },
    emailPlaceHolderStyle:{
        marginTop: WP('5')
    },
    detailOfInvitation:{
        fontWeight: 'normal', 
        color: colors.mediumGrey, 
        marginTop: WP('3'), 
        alignSelf: 'center', 
        fontSize: WP('3.2'), 
        marginHorizontal: WP('5') 
    },
    BtnStyle:{
        height:WP('10'),
        width:WP('30'),
        backgroundColor:colors.black,
        marginVertical: WP('5'), 
        alignSelf: 'center', 
        alignItems: 'center', 
        flexDirection: 'row', 
        justifyContent: 'center', 
        borderRadius: 30,   
    },
    addTextStyle:{
        color:colors.white, 
        alignSelf: 'center' 
    },
    addIconStyle:{
        height: WP('6'),
        width: WP('6'), 
        marginLeft: WP('5') 
    },
    //Card   
    cardContainer: {
        width: WP('90'), 
        flexDirection: 'row', 
        justifyContent: 'space-between', 
        flexWrap: 'wrap', 
        alignSelf: 'center', 
    },
    cardViewStyle:{
        height: WP('10'), 
        width: WP('43'), 
        justifyContent: 'center', 
        justifyContent: 'space-between', 
        alignItems: 'center', 
        borderRadius: WP('10'), 
        flexDirection: 'row', 
        marginTop: WP('2'), 
        borderWidth: WP('0.07'), 
        borderColor: colors.lightGrey, 
        backgroundColor: colors.white, 
    },
    cardViewTextStyle:{
        fontSize: WP('2.7'), 
        marginLeft: WP('3'), 
        color: colors.lightGrey 
    },
    cardViewIconContStyle:{
        height:WP('4'), 
        width:WP('4'), 
        marginRight:WP('2'), 
        alignItems:'center', 
        justifyContent:'center'
    },
    cardViewIconStyle:{
        height: WP('2'), 
        width: WP('3'), 
    },
    startCardBtnStyle: {
        alignSelf: 'center',
        marginBottom: WP('5'),
        marginTop: WP('5'),
        backgroundColor: colors.black,
        width: WP('25'),
        height: WP('10'),
    },
    confirmBtnStyle:{
        height: WP('10'), 
        width: WP('80'), 
        backgroundColor: colors.btnColorOrange, 
        borderRadius:30, 
        alignSelf: 'center', 
        alignItems:'center', 
        justifyContent: 'center', 
        marginTop:WP('5') 
    }
});