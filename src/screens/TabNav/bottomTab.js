import React from 'react';
import {Image } from 'react-native';
import { colors, WP, appImages } from '../../services';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { createStackNavigator } from 'react-navigation-stack';

import Home from '../mainFlow/Home';
import Calendar from '../mainFlow/Calendar';
import MyClasses from '../mainFlow/MyClasses';
import MyCourses from '../mainFlow/MyCourses';

export const PackageRequest = createStackNavigator({
    Home: Home,
    Calendar: Calendar,
    MyClasses: MyClasses,
    MyCourses: MyCourses
}, {
    headerMode: 'none'
});

export default createBottomTabNavigator(
    {
        Home: {
            screen: Home,
            navigationOptions: {
                title: 'Home'
            }
        },
        Calendar: {
            screen: Calendar,
            navigationOptions: {
                title: 'Calendar'
            }
        },
        MyClasses: {
            screen: MyClasses,
            navigationOptions: {
                title: 'My Classes'
            }
        },
        MyCourses: {
            screen: MyCourses,
            navigationOptions: {
                title: 'My Courses'
            }
        },
    },
    {
        tabBarOptions: {
            activeTintColor: colors.btnColorOrange,
            inactiveTintColor: colors.drakBlack,
        },
        defaultNavigationOptions: ({ navigation }) => ({
            tabBarIcon: ({ focused }) => {
                const { routeName } = navigation.state;
                if (routeName === 'Home') {
                    return (
                        focused ?
                            <Image
                                source={appImages.HomeIcon}
                                style={{ height: WP('6'), width: WP('6'), resizeMode: 'contain' }}
                            />
                            :
                            <Image
                                source={appImages.HomeIcon}
                                style={{ height: WP('6'), width: WP('6'), resizeMode: 'contain' }}
                            />
                    )
                } else if (routeName === 'Calendar') {
                    return (
                        focused ?
                            <Image
                                source={appImages.calenderIcon}
                                style={{ height: WP('6'), width: WP('6'), resizeMode: 'contain' }}
                            />
                            :
                            <Image
                                source={appImages.unselectedCalenderIcon}
                                style={{ height: WP('6'), width: WP('6'), resizeMode: 'contain' }}
                            />
                    )
                }
                else if (routeName === 'MyClasses') {
                    return (
                        focused ?
                            <Image
                                source={appImages.SelectStudentsIcon}
                                style={{ height: WP('6'), width: WP('6'), resizeMode: 'contain' }}
                            />
                            :
                            <Image
                                source={appImages.StudentsIcon}
                                style={{ height: WP('6'), width: WP('6'), resizeMode: 'contain' }}
                            />
                    )
                }
                else {
                    return (
                        focused ?
                            <Image
                                source={appImages.CapIcon}
                                style={{ height: WP('6'), width: WP('6'), resizeMode: 'contain' }}
                            />
                            :
                            <Image
                                source={appImages.CapIcon}
                                style={{ height: WP('6'), width: WP('6'), resizeMode: 'contain' }}
                            />
                    )
                }
            },
        }),
    }
);
