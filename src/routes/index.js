import React from 'react';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack'
import { createDrawerNavigator } from 'react-navigation-drawer';
//Login
import Login from '../screens/authFlow/login';
import ForgetPassword from '../screens/authFlow/resetPassword';
import NewPassword from '../screens/authFlow/newPassword';
// //SignUp
import SignUp from '../screens/authFlow/signup';
// ClassCreation
import MyClasses from '../screens/mainFlow/MyClasses';
import MyCourses from '../screens/mainFlow/MyCourses';
import ClassCreation from '../screens/mainFlow/ClassCeeation';
import CourseCreation from '../screens/mainFlow/CourseCreation';
// Profile
import Profile from '../screens/mainFlow/Profile';
//Tab stack
import TabStack from '../screens/TabNav/bottomTab';
//Home
import Home from '../screens/mainFlow/Home';
//MyArchives
import MyArchives from '../screens/mainFlow/MyArchives';
//AddStudents
import AddStudents from '../screens/mainFlow/AddStudents';
//List Of Students
import StudentsList from '../screens/mainFlow/ListOfStudents';
import Calender from '../screens/mainFlow/Calendar';

//AuthStack
export const AuthStack = createStackNavigator({
    Login: Login,
    SignUp: SignUp,
    TabStack: TabStack,
    Calender: Calender,
    ForgetPassword: ForgetPassword,
    NewPassword: NewPassword,
    MyClasses: MyClasses,
    MyCourses: MyCourses,
    ClassCreation: ClassCreation,
    CourseCreation: CourseCreation,
    Profile: Profile,
    Home: Home,
    MyArchives: MyArchives,
    AddStudents: AddStudents,
    ListOfStudents: StudentsList,
}, {
    headerMode: 'none',
    initialRouteName: 'Login'
});

export default createAppContainer(AuthStack);



