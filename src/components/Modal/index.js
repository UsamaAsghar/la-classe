import React from 'react'
import { View, StyleSheet } from 'react-native'
import { WP, colors } from '../../services';
import Modal from 'react-native-modal';
import AntIcon from 'react-native-vector-icons/AntDesign';
import { NormalText } from '../text'

export const CustomModal = props => {
  return (
    <Modal
      animationInTiming={200}
      animationOutTiming={100}
      animationIn="slideInLeft"
      animationOut="slideOutRight"
      avoidKeyboard={true}
      transparent={true}
      isVisible={props.showModal}
      onBackdropPress={() => props.toggleModal()}
      style={styles.modalContainer}
    >
      <View style={styles.modalStyle}>
        <View style={styles.iconContainer}>
          <AntIcon
            name={'checkcircle'}
            color={colors.btnColorOrange}
            size={40}
          />
        </View>
        <NormalText
          text={props.message}
          style={styles.textStyle}
        />
      </View>
    </Modal>
  );
}

const styles = StyleSheet.create({
  modalContainer: { 
    flex: 1, 
    justifyContent: 'center' 
  },
  modalStyle: { 
    width: WP('90'), 
    backgroundColor: colors.white, 
    alignSelf: 'center', 
    marginTop: WP('14'), 
    borderRadius: 5 
  },
  iconContainer: {
    alignItems: 'center', 
    marginTop: WP('6'), 
    marginBottom: WP('3')
  },
  textStyle: { 
    marginHorizontal: WP('5'), 
    textAlign: 'center', 
    marginBottom: WP('6') 
  }
})  