import React from 'react'
import { View, Text, Image, StyleSheet, TouchableOpacity, ActivityIndicator } from 'react-native'
import { WP, colors, size, family } from '../../services';
import Icon from 'react-native-vector-icons/FontAwesome';
import { styles } from '../../screens/mainFlow/MyCourses/styles';

    export const Button = props => {
    return (
        <View>
            {props.showLoader ?
                <View style={[buttonStyles.container, props.style]}
                >
                    <ActivityIndicator color="#fff" animating size={WP('10')} />
                </View>
                :
                <TouchableOpacity style={[buttonStyles.container, props.style]}
                    disabled={props.disabled}
                    onPress={props.onPress}
                >
                    <View style={buttonStyles.iconStyle}>
                        {props.isLeftShowIcon ?
                            <Image source={props.image} style={buttonStyles.leftStyle} />
                            : null}
                    </View>
                    <View style={[buttonStyles.textContainer, props.titleContainerStyle]}>
                        <Text style={[buttonStyles.btnText, props.titleStyle]}>{props.title}</Text>
                    </View>
                    <View style={buttonStyles.iconStyle}>
                        {props.isRightShowIcon ?
                            <Image source={props.image} style={buttonStyles.rightStyle} />
                            : null}
                    </View>
                </TouchableOpacity>
            }
        </View>
    );
}
const buttonStyles = StyleSheet.create({
    container: {
        display: 'flex',
        width: WP('90'),
        height: WP('13'),
        backgroundColor: colors.btnColorBlack,
        borderRadius: 5,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: WP('10')
    },
    textContainer: {
        alignItems: 'center',
    },
    btnText: {
        color: "#fff",
        // fontFamily: family.boldText,
        fontSize: WP('3.5'),
    },
    iconStyle: {
        width: WP('5'),
        marginHorizontal: 10,
    },
    leftStyle: {
        height: WP('5'),
        width: WP('5'),
        resizeMode: 'contain'
    },
    rightStyle: {
        height: WP('5'),
        width: WP('5'),
        resizeMode: 'contain'
    }
})

