import React from 'react';
import { View } from 'react-native';
import { WP, colors } from '../../services';
import { Platform ,StyleSheet, TextInput, Image } from 'react-native';

export const InputField = props => {
    return (
        <View style={[styles.inputContainer, props.styles]}>
            {
                props.isShowIcon?
                    <Image
                        source={props.image}
                        style={styles.imageStyle}
                    />
                    : null
            }   
            <TextInput
                value={props.value}
                placeholder={props.placeholder}
                placeholderTextColor={colors.mediumGrey}
                secureTextEntry={props.secureTextEntry}
                onChangeText={(value)=> props.onChangeText(value)}
                style={styles.inputStyle}
            />
        </View>
    );
}

const styles = StyleSheet.create({
    inputContainer: {
        height: WP('13'),
        width: WP('90'),
        fontFamily: 'Roboto-Regular',
        fontSize: WP('3.8'),
        flexDirection: 'row',
        paddingVertical: 0,
        alignSelf: 'center',
        borderRadius: WP('1.5'),
        backgroundColor: colors.white,
        ...Platform.select({
            ios: {
                shadowColor: '#000',
                shadowOffset: { width: 0, height: 2 },
                shadowOpacity: 0.2,
            },
            android: {
                elevation: 1
            },
        }),
    },
    inputStyle: {
        height: WP('12'),
        width: WP('70'),
        alignSelf:'center',
        marginLeft: WP('3'),
    },
    imageStyle: {
        height: WP('7'), 
        width: WP('7'), 
        resizeMode: 'contain',
        alignSelf: 'center',
        marginLeft: WP('3')
    }
});