import React from 'react';
import { View, Image, TouchableOpacity, Text } from 'react-native';
import { NormalText } from '../text';
import { Avatar } from 'react-native-elements';
import { appImages } from '../../services'
import { styles } from './styles'

export const NormalHeader = props => {
    return (
        <View style={styles.profileHeader}>
            <TouchableOpacity style={styles.headerImgContStyle}>
                <Avatar
                    onPress={props.onPressProfile}
                    rounded
                    size="medium"
                    source={appImages.userProfileIcon}
                />
            </TouchableOpacity>
            <View style={styles.nameCentStyle}>
                <NormalText
                    text={props.userName}
                    style={styles.nameStyle}
                />
                <NormalText
                    text={props.email}
                    style={styles.emailStyle}
                />
            </View>
            <TouchableOpacity
                onPress={props.setShowModal}
            >
                <Image
                    source={appImages.notificationIcon}
                    style={styles.notificationIcon}
                />
                <View style={styles.notifTag}>
                    <Text style={styles.tagTextStyle}>{props.notifications ? props.notifications.length : 0}</Text>
                </View>
            </TouchableOpacity>
        </View>
    );
}

