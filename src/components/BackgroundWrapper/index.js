import React, { useState } from 'react';
import { View, StyleSheet, Image, TouchableOpacity, Text, ScrollView } from 'react-native';
import { MediumText, NormalText} from '../text';
import { NotificationCard } from '../cards';
import { Loader } from '../loader';
import { Avatar } from 'react-native-elements';
import { appImages, registorStrings} from '../../services'
import { WP, HP, colors } from '../../services';
import Modal from 'react-native-modal';
import { styles } from './styles';
import { NormalHeader } from './NormalHeader';
import { BackBtnHeader } from './BackBtnHeader';

export const BackgroundWarapper = props => {
    const [isShowModal, setShowModal] = useState(false);
    const [dummyArr] = useState([{}, {}, {}, {}, {}, {}, {}, {}]);

    return (
        <View style={styles.container}>
            {
                props.isLogin ?
                    <View style={styles.loginHeader}>
                        <Image
                            source={appImages.applogo}
                            style={styles.logoStyle}
                        />
                    </View>
                    :
                    props.isRegister ?
                        <View style={styles.registerHeader}>
                            <MediumText
                                text={registorStrings.headerText}
                                style={styles.titleStyle}
                            />
                            <NormalText
                                text={registorStrings.subTitle}
                                style={styles.subTitleStyle}
                            />
                        </View>
                        :
                        props.isProfile ?
                            props.isBackBtn ?
                                <BackBtnHeader
                                    {...props}
                                    setShowModal={() => setShowModal(!isShowModal)}
                                />
                                :
                                <NormalHeader
                                    {...props}
                                    setShowModal={() => setShowModal(!isShowModal)}
                                />
                            :
                            null
            }
            <View style={[styles.chlid, props.style]}>
                {props.isLoading ?
                        <View style={styles.loaderContainer}>
                            <Loader />
                        </View>
                        :
                        props.children
                }
            </View>
            <Modal
                animationInTiming={300}
                animationOutTiming={200}
                animationIn="flipInY"
                animationOut="flipOutY"
                backdropOpacity={0.1}
                avoidKeyboard={true}
                transparent={true}
                isVisible={isShowModal}
                onBackdropPress={() => setShowModal(false)}
                style={{ flex: 1, justifyContent: 'flex-start',marginTop: HP('7') }}
            >
                <View style={styles.modalCont}>
                    <View style={styles.titleTextCon}>
                        <NormalText
                            text={'Notifications'}
                            style={styles.titleTextStyle}
                        />
                    </View>
                    <ScrollView
                        indicatorStyle='black'
                        scrollIndicatorInsets={{ top: 5, left: 0, bottom: 5, right: 0 }}
                    >
                        {
                            props.notifications && props.notifications.length > 0 ?
                                props.notifications.map((item, key) => {
                                    return (
                                        <NotificationCard
                                            key={key}
                                            item={item}
                                            onPress={() => setShowModal(false)}
                                        />
                                    )
                                })
                                :
                                <View style={{ height: HP('70'), justifyContent: 'center', alignItems: 'center' }}>
                                    <Text style={{ fontSize: WP('4'), color: colors.mediumGrey, alignSelf: 'center' }}>Empty</Text>
                                </View>
                        }
                    </ScrollView>
                </View>
            </Modal>
        </View>
    );
}

// const styles = StyleSheet.create({
//     container: {
//         flex: 1,
//         backgroundColor: colors.p_bgColor
//     },
//     chlid: {
//         flex: 1,
//         backgroundColor: colors.c_bgColor,
//         borderTopLeftRadius: WP('10'),
//         borderTopRightRadius: WP('10')
//     },
//     loginHeader: {
//         height: WP('50'),
//         width: WP('100'),
//         alignItems: 'center',
//         justifyContent: 'center'
//     },
//     logoStyle: {
//         height: WP('20'),
//         width: WP('60'),
//         resizeMode: 'contain'
//     },
//     registerHeader: {
//         height: WP('30'),
//         width: WP('100'),
//         alignItems: 'center',
//         justifyContent: 'center'
//     },
//     titleStyle: {
//         alignSelf: 'center',
//         marginTop: WP('4'),
//         color: colors.drakBlack,
//     },
//     subTitleStyle: {
//         alignSelf: 'center',
//         marginTop: WP('1'),
//         color: colors.mediumGrey
//     },
//     profileHeader: {
//         height: WP('25'),
//         width: WP('100'),
//         flexDirection: 'row',
//         alignItems: 'center'
//     },
//     headerImgContStyle: {
//         alignSelf: 'center',
//         marginLeft: WP('5'),
//         justifyContent: 'center'
//     },
//     headerImgStyle: {
//         height: WP('15'),
//         width: WP('15'),
//         justifyContent: 'center',
//     },
//     nameCentStyle: {
//         width: WP('60'),
//         alignSelf: 'center',
//         marginHorizontal: WP('3'),
//         justifyContent: 'center'
//     },
//     nameStyle: {
//         color: colors.drakBlack,
//         fontSize: WP('4.5')
//     },
//     emailStyle: {
//         color: colors.mediumGrey,
//         fontSize: WP('3.5')
//     },
//     notificationIcon: {
//         height: WP('8'),
//         width: WP('8'),
//         resizeMode: 'contain',
//         marginLeft: WP('2')
//     },
//     notifTag: {
//         height: WP('5'),
//         width: WP('5'),
//         position: 'absolute',
//         right: -5,
//         bottom: 0,
//         borderRadius: WP('30'),
//         alignItems: 'center',
//         justifyContent: 'center',
//         backgroundColor: colors.btnColorOrange
//     },
//     tagTextStyle: {
//         fontSize: WP('2.5'),
//         color: colors.black,
//     },
//     loaderContainer: { 
//         flex: 1, 
//         justifyContent: 'center' 
//     },
//     //Modal Style
//     modalCont: {
//         height: HP('80'),
//         width: WP('90'),
//         backgroundColor: colors.white,
//         borderRadius: WP('2'),
//         overflow: 'hidden',
//         // marginTop: WP('5'), 
//     },
//     titleTextCon: {
//         height: WP('12'),
//         width: WP('90'),
//         backgroundColor: colors.btnColorOrange,
//         justifyContent: 'center'
//     },
//     titleTextStyle: {
//         marginHorizontal: WP('5'),
//         color: colors.lightBlack
//     },
//     // ClassCard

// });

