import { StyleSheet } from 'react-native'
import { WP, HP, colors } from '../../services';

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.p_bgColor
    },
    chlid: {
        flex: 1,
        backgroundColor: colors.c_bgColor,
        borderTopLeftRadius: WP('10'),
        borderTopRightRadius: WP('10')
    },
    loginHeader: {
        height: WP('50'),
        width: WP('100'),
        alignItems: 'center',
        justifyContent: 'center'
    },
    logoStyle: {
        height: WP('20'),
        width: WP('60'),
        resizeMode: 'contain'
    },
    registerHeader: {
        height: WP('30'),
        width: WP('100'),
        alignItems: 'center',
        justifyContent: 'center'
    },
    titleStyle: {
        alignSelf: 'center',
        marginTop: WP('4'),
        color: colors.drakBlack,
    },
    subTitleStyle: {
        alignSelf: 'center',
        marginTop: WP('1'),
        color: colors.mediumGrey
    },
    profileHeader: {
        height: WP('25'),
        width: WP('100'),
        flexDirection: 'row',
        alignItems: 'center'
    },
    headerImgContStyle: {
        alignSelf: 'center',
        marginLeft: WP('5'),
        justifyContent: 'center'
    },
    headerImgStyle: {
        height: WP('15'),
        width: WP('15'),
        justifyContent: 'center',
    },
    nameCentStyle: {
        width: WP('60'),
        alignSelf: 'center',
        marginHorizontal: WP('3'),
        justifyContent: 'center'
    },
    nameStyle: {
        color: colors.drakBlack,
        fontSize: WP('4.5')
    },
    emailStyle: {
        color: colors.mediumGrey,
        fontSize: WP('3.5')
    },
    notificationIcon: {
        height: WP('8'),
        width: WP('8'),
        resizeMode: 'contain',
        marginLeft: WP('2')
    },
    notifTag: {
        height: WP('5'),
        width: WP('5'),
        position: 'absolute',
        right: -5,
        bottom: 0,
        borderRadius: WP('30'),
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: colors.btnColorOrange
    },
    tagTextStyle: {
        fontSize: WP('2.5'),
        color: colors.black,
    },
    loaderContainer: { 
        flex: 1, 
        justifyContent: 'center' 
    },
    //Modal Style
    modalCont: {
        height: HP('80'),
        width: WP('90'),
        backgroundColor: colors.white,
        borderRadius: WP('2'),
        overflow: 'hidden',
        // marginTop: WP('5'), 
    },
    titleTextCon: {
        height: WP('12'),
        width: WP('90'),
        backgroundColor: colors.btnColorOrange,
        justifyContent: 'center'
    },
    titleTextStyle: {
        marginHorizontal: WP('5'),
        color: colors.lightBlack
    },
    // ClassCard

});

