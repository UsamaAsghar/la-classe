import React from 'react';
import { View, Image, TouchableOpacity, Text } from 'react-native';
import { NormalText } from '../text';
import BackArrowIcon from 'react-native-vector-icons/AntDesign';
import { Avatar } from 'react-native-elements';
import { appImages, colors, WP } from '../../services'
import { styles } from './styles'

export const BackBtnHeader = props => {
    return (
        <View style={styles.profileHeader}>
            <TouchableOpacity 
                style={[styles.headerImgContStyle,{ flexDirection: 'row' }]}
                onPress={props.onBackBtn}
                >
                <BackArrowIcon 
                    name="left" 
                    size={26} 
                    color={colors.lightBlack} 
                    style={{ alignSelf: 'center' }}
                />
                <Avatar
                    // onPress={props.onPressProfile}
                    rounded
                    size="small"
                    source={appImages.userProfileIcon}
                />
            </TouchableOpacity>
            <View style={[styles.nameCentStyle,{ width: WP('57') }]}>
                <NormalText
                    text={props.userName}
                    style={[styles.nameStyle,{ bottom: -3 }]}
                />
                <NormalText
                    text={props.email}
                    style={[styles.emailStyle,{ top: -3 }]}
                />
            </View>
            <TouchableOpacity
                onPress={props.setShowModal}
            >
                <Image
                    source={appImages.notificationIcon}
                    style={styles.notificationIcon}
                />
                <View style={styles.notifTag}>
                    <Text style={styles.tagTextStyle}>{props.notifications ? props.notifications.length : 0}</Text>
                </View>
            </TouchableOpacity>
        </View>
    );
}

