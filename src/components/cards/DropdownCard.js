import React from 'react'
import { Platform, View, StyleSheet } from 'react-native'
import { WP, colors } from '../../services'
import DropDownIcon from 'react-native-vector-icons/AntDesign'
import RNPickerSelect from 'react-native-picker-select';

export const DropDownCard = props => {
    return (
        <View style={[styles.container, props.style]}>
            <View style={[styles.dropdownContainer, props.dropdownContainer]}>
                <View style={{ flex: 1, marginHorizontal: WP(2) }}>
                    <RNPickerSelect
                        // placeholder={{ label: props.placeholderText }}
                        placeholderTextColor={colors.lightGrey}
                        useNativeAndroidPickerStyle={false}
                        value={props.value}
                        disabled={props.disabled}
                        onValueChange={(value, index) => props.onSelectItem(index, value)}
                        items={props.dropDownOptions}
                        style={pickerSelectStyles}
                    ></RNPickerSelect>
                </View>
                {
                    props.hideIcon ?
                        null
                        :
                        <DropDownIcon
                            name='caretdown'
                            size={10}
                            color='black'
                            style={[styles.iconStyle, props.iconStyle]}
                        />
                }
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        height: WP('13'),
        width: WP('90'),
        flexDirection: 'row',
        alignSelf: 'center',
        borderRadius: WP('1.5'),
        backgroundColor: colors.white,
        ...Platform.select({
            ios: {
                shadowColor: '#000',
                shadowOffset: { width: 0, height: 2 },
                shadowOpacity: 0.2,
            },
            android: {
                elevation: 1
            },
        }),
    },
    dropdownContainer: {
        width: WP('90'),
        height: WP('13'),
        position: 'relative',
        flexDirection: 'row',
        alignItems: 'center',
        marginHorizontal: WP('5'),
    },
    iconStyle: {
        marginHorizontal: WP('3'),
        zIndex: 1,
        position: 'absolute',
        right: 25
    }
})

const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
        // fontSize: 16,
        color: colors.black,
        backgroundColor: 'transparent',
        left: -17,
        paddingRight: 0, // to ensure the text is never behind the icon
    },
    inputAndroid: {
        // fontSize: 16,
        color: colors.black,
        paddingRight: 30, // to ensure the text is never behind the icon
    },
});
