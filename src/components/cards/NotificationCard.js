
import React from 'react';
import { View, StyleSheet, TouchableOpacity, Image } from 'react-native';
import { WP, colors, appImages } from '../../services';
import { MediumText, NormalText, SmallText } from '../text';

export const NotificationCard = props => {
    return (
        <TouchableOpacity
            style={[styles.container, props.style]}
            onPress={props.onPress}
        >
            <View style={{ flexDirection: 'row', marginVertical: WP('4') }}>
                <View style={styles.ImgCont}>
                    <Image
                        source={appImages.CapIcon}
                        style={styles.ImgStyle}
                    />
                </View>
                <View style={styles.textConStyle}>
                    <View style={{ flexDirection: 'row', flexWrap: 'wrap', width: WP('67'), marginRight: 5 }}>
                        <SmallText
                            text={props.item.message}
                            style={styles.invitationTextStyle}
                        />
                        <SmallText
                            text={'Oscar L. Valention '}
                            style={styles.nameTextStyle}
                        />
                    </View>
                    <SmallText
                        text={'23m ago'}
                        style={styles.timeTextStyle}
                    />
                </View>
            </View>
            <View style={styles.underLineStyle} />
            <View style={{ marginBottom: WP('2') }}></View>
        </TouchableOpacity>
    );
}

const styles = StyleSheet.create({
    container: {
        width: WP('90'),
        backgroundColor: colors.white,
        justifyContent: 'center'
    },
    ImgCont: {
        height: WP('12'),
        width: WP('12'),
        marginHorizontal: WP('3'),
        borderRadius: WP('30'),
        justifyContent: 'center',
        backgroundColor: colors.btnColorOrange
    },
    ImgStyle: {
        height: WP('8'),
        width: WP('8'),
        resizeMode: 'contain',
        alignSelf: 'center'
    },
    textConStyle: {
        justifyContent: 'center'
    },
    invitationTextStyle: {
        fontSize: WP('3'),
        color: colors.mediumGrey
    },
    nameTextStyle: {
        fontSize: WP('3')
    },
    timeTextStyle: {
        fontSize: WP('3'),
        color: colors.lightGrey
    },
    underLineStyle: {
        height: WP('0.1'),
        width: WP('80'),
        backgroundColor: colors.lightGrey,
        alignSelf: 'center'
    }
})
